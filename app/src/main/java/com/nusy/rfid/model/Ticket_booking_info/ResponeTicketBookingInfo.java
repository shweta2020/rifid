
package com.nusy.rfid.model.Ticket_booking_info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponeTicketBookingInfo {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<Datai> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Datai> getData() {
        return data;
    }

    public void setData(List<Datai> data) {
        this.data = data;
    }


    public class Datai {

        @SerializedName("event_name")
        @Expose
        private String eventName;
        @SerializedName("event_venue")
        @Expose
        private String eventVenue;
        @SerializedName("event_time")
        @Expose
        private String eventTime;
        @SerializedName("ticket")
        @Expose
        private Ticket ticket;

        public String getEventName() {
            return eventName;
        }

        public void setEventName(String eventName) {
            this.eventName = eventName;
        }

        public String getEventVenue() {
            return eventVenue;
        }

        public void setEventVenue(String eventVenue) {
            this.eventVenue = eventVenue;
        }

        public String getEventTime() {
            return eventTime;
        }

        public void setEventTime(String eventTime) {
            this.eventTime = eventTime;
        }

        public Ticket getTicket() {
            return ticket;
        }

        public void setTicket(Ticket ticket) {
            this.ticket = ticket;
        }

    }

    public class Ticket {

        @SerializedName("name")
        @Expose
        private Object name;
        @SerializedName("qty")
        @Expose
        private String qty;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("sub_total")
        @Expose
        private Integer subTotal;
        @SerializedName("parking")
        @Expose
        private String parking;
        @SerializedName("total")
        @Expose
        private Integer total;

        public Object getName() {
            return name;
        }

        public void setName(Object name) {
            this.name = name;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public Integer getSubTotal() {
            return subTotal;
        }

        public void setSubTotal(Integer subTotal) {
            this.subTotal = subTotal;
        }

        public String getParking() {
            return parking;
        }

        public void setParking(String parking) {
            this.parking = parking;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

    }
}
