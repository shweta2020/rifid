package com.nusys.rfid.model.Food

data class singleFood(
        val `data`: List<Data>,
        val msg: String,
        val status: Int
)

data class Data(
        val amount: String,
        val food_id: String,
        val food_item: String,
        val food_type: String,
        val image: String
)