
package com.nusy.rfid.model.Response_my_booking;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponeBook {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {
        @SerializedName("event_date")
        @Expose
        private String eventDate;
        @SerializedName("order_id")
        @Expose
        private String orderId;
        @SerializedName("event_id")
        @Expose
        private String eventId;
        @SerializedName("event_name")
        @Expose
        private String eventName;
        @SerializedName("event_venue")
        @Expose
        private String eventVenue;
        @SerializedName("event_time")
        @Expose
        private String eventTime;
        @SerializedName("ticket")
        @Expose
        private Ticket ticket;
        public String getOrderId() {
            return orderId;
        }
        public String getEventDate() {
            return eventDate;
        }

        public void setEventDate(String eventDate) {
            this.eventDate = eventDate;
        }
        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }
        public String getEventId() {
            return eventId;
        }

        public void setEventId(String eventId) {
            this.eventId = eventId;
        }
        public String getEventName() {
            return eventName;
        }

        public void setEventName(String eventName) {
            this.eventName = eventName;
        }

        public String getEventVenue() {
            return eventVenue;
        }

        public void setEventVenue(String eventVenue) {
            this.eventVenue = eventVenue;
        }

        public String getEventTime() {
            return eventTime;
        }

        public void setEventTime(String eventTime) {
            this.eventTime = eventTime;
        }

        public Ticket getTicket() {
            return ticket;
        }

        public void setTicket(Ticket ticket) {
            this.ticket = ticket;
        }

    }

    public class Parking implements CharSequence {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("qty")
        @Expose
        private String qty;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("parking_total")
        @Expose
        private Integer parkingTotal;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public Integer getParkingTotal() {
            return parkingTotal;
        }

        public void setParkingTotal(Integer parkingTotal) {
            this.parkingTotal = parkingTotal;
        }

        @Override
        public int length() {
            return 0;
        }

        @Override
        public char charAt(int index) {
            return 0;
        }

        @NonNull
        @Override
        public CharSequence subSequence(int start, int end) {
            return null;
        }
    }

    public class Ticket {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("qty")
        @Expose
        private String qty;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("sub_total")
        @Expose
        private Integer subTotal;
        @SerializedName("parking")
        @Expose
        private Parking parking;
        @SerializedName("total")
        @Expose
        private Integer total;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public Integer getSubTotal() {
            return subTotal;
        }

        public void setSubTotal(Integer subTotal) {
            this.subTotal = subTotal;
        }

        public Parking getParking() {
            return parking;
        }

        public void setParking(Parking parking) {
            this.parking = parking;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

    }
}