package com.nusy.rfid.model.Parking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponeParking {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<Datap> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Datap> getData() {
        return data;
    }

    public void setData(List<Datap> data) {
        this.data = data;
    }
    public class Datap {

        @SerializedName("parking_price_id")
        @Expose
        private String parkingPriceId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("no_parking")
        @Expose
        private String noParking;
        @SerializedName("amount")
        @Expose
        private String amount;

        public String getParkingPriceId() {
            return parkingPriceId;
        }

        public void setParkingPriceId(String parkingPriceId) {
            this.parkingPriceId = parkingPriceId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNoParking() {
            return noParking;
        }

        public void setNoParking(String noParking) {
            this.noParking = noParking;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

    }
}