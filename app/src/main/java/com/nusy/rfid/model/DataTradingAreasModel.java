package com.nusy.rfid.model;

public class DataTradingAreasModel {

    private int img;

    private String titles;


    public DataTradingAreasModel(int img, String titles) {
        this.img = img;
        this.titles = titles;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getTitles() {
        return titles;
    }

    public void setTitles(String titles) {
        this.titles = titles;
    }
}
