package com.nusy.rfid.model;

public class EventDataModel {



    private String id;
    private String  event_name;
    private String  event_time;
    private String  start_date;
    private String  end_date;
    private String  event_address;
    private String  max_visitor;
    private String  event_type;
    private String  amount;
    private String  image;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_time() {
        return event_time;
    }

    public void setEvent_time(String event_time) {
        this.event_time = event_time;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }
    public String getEvent_address() {
        return event_address;
    }

    public void setEvent_address(String event_addres) {
        this.event_address = event_addres;
    }

    public String getMax_visitor() {
        return max_visitor;
    }

    public void setMax_visitor(String max_visitor) {
        this.max_visitor = max_visitor;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
