package com.nusy.rfid.model.Food;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponeFoodItemStatus {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<Datafl> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Datafl> getData() {
        return data;
    }

    public void setData(List<Datafl> data) {
        this.data = data;
    }

    public class Datafl {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("food_name")
        @Expose
        private String foodName;
        @SerializedName("quantity")
        @Expose
        private String quantity;
        @SerializedName("payment_status")
        @Expose
        private String paymentStatus;
        @SerializedName("payment_type")
        @Expose
        private String paymentType;
        @SerializedName("total_amount")
        @Expose
        private String totalAmount;
        @SerializedName("status")
        @Expose
        private Object status;
        @SerializedName("remark")
        @Expose
        private Object remark;
        @SerializedName("event_name")
        @Expose
        private String eventName;
        @SerializedName("event_address")
        @Expose
        private String eventAddress;
        @SerializedName("event_venue")
        @Expose
        private String eventVenue;
        @SerializedName("time")
        @Expose
        private String time;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFoodName() {
            return foodName;
        }

        public void setFoodName(String foodName) {
            this.foodName = foodName;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getPaymentStatus() {
            return paymentStatus;
        }

        public void setPaymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(String totalAmount) {
            this.totalAmount = totalAmount;
        }

        public String getStatus() {
            return (String) status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRemark() {
            return (String) remark;
        }

        public void setRemark(Object remark) {
            this.remark = remark;
        }

        public String getEventName() {
            return eventName;
        }

        public void setEventName(String eventName) {
            this.eventName = eventName;
        }

        public String getEventAddress() {
            return eventAddress;
        }

        public void setEventAddress(String eventAddress) {
            this.eventAddress = eventAddress;
        }

        public String getEventVenue() {
            return eventVenue;
        }

        public void setEventVenue(String eventVenue) {
            this.eventVenue = eventVenue;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

    }

}
