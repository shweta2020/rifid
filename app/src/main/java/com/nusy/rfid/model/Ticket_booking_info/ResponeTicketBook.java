
package com.nusy.rfid.model.Ticket_booking_info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponeTicketBook {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<Datat> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Datat> getData() {
        return data;
    }

    public void setData(List<Datat> data) {
        this.data = data;
    }



    public class Datat {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("order_id")
        @Expose
        private String orderId;
        @SerializedName("invoice_no")
        @Expose
        private Object invoiceNo;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("phone_no")
        @Expose
        private String phoneNo;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("pincode")
        @Expose
        private String pincode;
        @SerializedName("paymeny_type")
        @Expose
        private String paymenyType;
        @SerializedName("total_amount")
        @Expose
        private String totalAmount;
        @SerializedName("transaction_id")
        @Expose
        private String transactionId;
        @SerializedName("transaction_status")
        @Expose
        private String transactionStatus;
        @SerializedName("paymeny_status")
        @Expose
        private String paymenyStatus;
        @SerializedName("order_status")
        @Expose
        private String orderStatus;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("event_detail")
        @Expose
        private List<EventDetail> eventDetail = null;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public Object getInvoiceNo() {
            return invoiceNo;
        }

        public void setInvoiceNo(Object invoiceNo) {
            this.invoiceNo = invoiceNo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getPaymenyType() {
            return paymenyType;
        }

        public void setPaymenyType(String paymenyType) {
            this.paymenyType = paymenyType;
        }

        public String getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(String totalAmount) {
            this.totalAmount = totalAmount;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

        public String getTransactionStatus() {
            return transactionStatus;
        }

        public void setTransactionStatus(String transactionStatus) {
            this.transactionStatus = transactionStatus;
        }

        public String getPaymenyStatus() {
            return paymenyStatus;
        }

        public void setPaymenyStatus(String paymenyStatus) {
            this.paymenyStatus = paymenyStatus;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public List<EventDetail> getEventDetail() {
            return eventDetail;
        }

        public void setEventDetail(List<EventDetail> eventDetail) {
            this.eventDetail = eventDetail;
        }
        public class EventDetail {

            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("event_name")
            @Expose
            private String eventName;
            @SerializedName("event_time")
            @Expose
            private String eventTime;
            @SerializedName("event_address")
            @Expose
            private String eventAddress;
            @SerializedName("slug")
            @Expose
            private String slug;
            @SerializedName("event_venue")
            @Expose
            private String eventVenue;
            @SerializedName("image")
            @Expose
            private String image;
            @SerializedName("description")
            @Expose
            private String description;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getEventName() {
                return eventName;
            }

            public void setEventName(String eventName) {
                this.eventName = eventName;
            }

            public String getEventTime() {
                return eventTime;
            }

            public void setEventTime(String eventTime) {
                this.eventTime = eventTime;
            }

            public String getEventAddress() {
                return eventAddress;
            }

            public void setEventAddress(String eventAddress) {
                this.eventAddress = eventAddress;
            }

            public String getSlug() {
                return slug;
            }

            public void setSlug(String slug) {
                this.slug = slug;
            }

            public String getEventVenue() {
                return eventVenue;
            }

            public void setEventVenue(String eventVenue) {
                this.eventVenue = eventVenue;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

        }
    }

}
