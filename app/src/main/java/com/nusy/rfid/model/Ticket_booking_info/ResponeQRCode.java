package com.nusy.rfid.model.Ticket_booking_info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponeQRCode {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<Dataqr> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Dataqr> getData() {
        return data;
    }

    public void setData(List<Dataqr> data) {
        this.data = data;
    }

    public class Dataqr {
        @SerializedName("qr_for")
        @Expose
        private String qrFor;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("file_name")
        @Expose
        private String fileName;
        @SerializedName("status")
        @Expose

        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
        public String getQrFor() {
            return qrFor;
        }

        public void setQrFor(String qrFor) {
            this.qrFor = qrFor;
        }
    }

}
