package com.nusy.rfid.model.Food;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponeFoodMenu {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<Dataf> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Dataf> getData() {
        return data;
    }

    public void setData(List<Dataf> data) {
        this.data = data;
    }

    public class Dataf {
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("food_id")
        @Expose
        private String foodId;
        @SerializedName("food_type")
        @Expose
        private String foodType;
        @SerializedName("food_item")
        @Expose
        private String foodItem;
        @SerializedName("amount")
        @Expose

        private String amount;
        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
        public String getFoodId() {
            return foodId;
        }

        public void setFoodId(String foodId) {
            this.foodId = foodId;
        }

        public String getFoodType() {
            return foodType;
        }

        public void setFoodType(String foodType) {
            this.foodType = foodType;
        }

        public String getFoodItem() {
            return foodItem;
        }

        public void setFoodItem(String foodItem) {
            this.foodItem = foodItem;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

    }

}
