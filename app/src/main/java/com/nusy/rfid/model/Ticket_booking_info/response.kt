package com.nusys.rfid.model.Ticket_booking_info

data class response(
    val `data`: Data,
    val msg: String,
    val status: Int
)

data class Data(
    val event_name: String,
    val event_time: String,
    val event_venue: String,
    val ticket: Ticket
)

data class Ticket(
    val amount: String,
    val name: Any,
    val parking: String,
    val qty: String,
    val sub_total: Int,
    val total: Int
)