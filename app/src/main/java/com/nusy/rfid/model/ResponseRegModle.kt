package com.nusys.rfid.model

data class ResponseRegModle(
    val `data`: List<Datar>,
    val msg: String,
    val status: Int,
    val token: String
)

data class Datar(
    val email: String,
    val id: String,
    val image: String,
    val name: String
)