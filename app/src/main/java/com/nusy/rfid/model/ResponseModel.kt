package com.nusys.rfid.model

data class ResponseModel(
    val `data`: List<Data>,
    val status: Int,
    val token: String
)

data class Data(
    val email: String,
    val id: String,
    val event_id: String,
    val image: String,
    val name: String,
    val total_items: Int,
    val amount: String,
    val end_date: String,
    val event_address: String,
    val event_name: String,
    val event_time: String,
    val event_type: String,
    val max_visitor: String,
    val start_date: String,
    val event_venue :String,
    val date :String,
    val date_in_time :String,
    val availability :String,
    val msg :String,
    val event_date :String,
    val ticket :String,
    val ticket_id :String,
    val seat :String,
    val status :String


)

