package com.nusy.rfid;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.nusy.rfid.commonMudules.NetworkUtils;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.retrofit.ApiClient;
import com.nusy.rfid.retrofit.ServiceInterface;
import com.nusy.rfid.utils.DialogsUtils;
import com.nusys.rfid.model.ResponseRegModle;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    EditText name, email, password, mobile;
    Button signUpButton;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String postRegisterUrl = "http://13.233.162.24/rifid/api/web_api/visitor_registration";
    String nameValue, emailValue, passwordValue, mobileValue;
    SharedPreference_main sharedPreference_main;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        name = findViewById(R.id.txtName);
        email = findViewById(R.id.txtEmailAddress);
        password = findViewById(R.id.txtConfirm_Password);
        mobile = findViewById(R.id.txtPhone);
        signUpButton = findViewById(R.id.button_Register);

        sharedPreference_main = SharedPreference_main.getInstance(this);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nameValue = name.getText().toString();
                emailValue = email.getText().toString();
                passwordValue = password.getText().toString();
                mobileValue = mobile.getText().toString();

                checkvalid();

            }
        });


    }

    private void checkvalid() {

        if (name.getText().toString().isEmpty()) {

            Toast.makeText(this, "enter Name:", Toast.LENGTH_SHORT).show();

        } else if (!email.getText().toString().matches(emailPattern)) {
            Toast.makeText(this, "enter Valid Email:", Toast.LENGTH_SHORT).show();

        } else if (password.getText().toString().isEmpty()) {

            Toast.makeText(this, "password must be entered", Toast.LENGTH_SHORT).show();

        } else {

            registerApi();

        }


    }

    private void registerApi() {
        progressDialog = DialogsUtils.showProgressDialog(SignUpActivity.this, "Please wait...");
        HashMap<String, String> map = new HashMap<>();
        map.put("name", nameValue);
        map.put("email", emailValue);
        map.put("mobile_no", mobileValue);
        map.put("password", passwordValue);
        if (NetworkUtils.isConnected(SignUpActivity.this)) {
            progressDialog.show();
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponseRegModle> call = serviceInterface.register(map);
            call.enqueue(new Callback<ResponseRegModle>() {
                @Override
                public void onResponse(Call<ResponseRegModle> call, Response<ResponseRegModle> response) {
                    if (response.isSuccessful()) {
                        progressDialog.show();
                        final ResponseRegModle bean = response.body();

                        if (bean.getStatus() == 200) {
                            progressDialog.dismiss();
                            sharedPreference_main.setIs_LoggedIn(true);
                            sharedPreference_main.setToken(bean.getToken());
                            sharedPreference_main.setId(Integer.parseInt(bean.getData().get(0).getId()));
                            sharedPreference_main.setemail(bean.getData().get(0).getEmail());
                            sharedPreference_main.setImage(bean.getData().get(0).getImage());
                            sharedPreference_main.setUsername(bean.getData().get(0).getName());
                            Toast.makeText(SignUpActivity.this, "You have Successfully Registered", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                        }else {
                            Toast.makeText(SignUpActivity.this,bean.getMsg(),Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                            //Toast.makeText(SignUpActivity.this, "success", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(SignUpActivity.this,"Some Thing Is Wrong Please Try Again",Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                        //Toast.makeText(SignUpActivity.this, "success", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseRegModle> call, Throwable t) {
                    Toast.makeText(SignUpActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("error", t.getMessage());
                    progressDialog.dismiss();
                }
            });
        }else {
            Toast.makeText(SignUpActivity.this, "Please Check the Internet Connection", Toast.LENGTH_LONG).show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            }, 2800);
        }
        /*call.enqueue(new Callback<ResponeRegistration>() {
            @Override
            public void onResponse(Call<ResponeRegistration> call, Response<ResponeRegistration> response) {
*//*                if (response.isSuccessful()) {
                    final ResponeRegistration bean = response.body();

                    if (bean.getStatus() == 200) {
                      *//**//*  sharedPreference_main.setIs_LoggedIn(true);
                        sharedPreference_main.setToken(bean.getToken());
                        sharedPreference_main.setId(Integer.parseInt(bean.getData().get(0).getId()));
                        sharedPreference_main.setemail(bean.getData().get(0).getEmail());
                        sharedPreference_main.setImage(bean.getData().get(0).getImage());
                        sharedPreference_main.setUsername(bean.getData().get(0).getName());*//**//*
                        Toast.makeText(SignUpActivity.this, "success", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                    }
                    //startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                   *//**//* Toast.makeText(SignUpActivity.this, "success", Toast.LENGTH_SHORT).show();

                    startActivity(new Intent(SignUpActivity.this, MainActivity.class));*//**//*

                } else {

                    Toast.makeText(SignUpActivity.this, "error", Toast.LENGTH_SHORT).show();
                }*//*
            }

            @Override
            public void onFailure(Call<ResponeRegistration> call, Throwable t) {

            }


        });*/
    }
   /* private void registerApi() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, postRegisterUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.e("TAG", "LEAD_jsonObject" + jsonObject);
                            String token = jsonObject.getString("token");
                            Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                            intent.putExtra("tokenValue", token);
                            startActivity(intent);
                           *//* String result1 =jsonObject.getString("status");
                            if(result1.equals("200")){
                            }else{
                                String result =jsonObject.getString("msg");
                                Toast.makeText(SignUpActivity.this, ""+result, Toast.LENGTH_SHORT).show();
                            }
*//*
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("JSON Parser", "Error parsing data [" + e.getMessage() + "] " + response);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("TAG", "onErrorResponse: " + String.valueOf(error));
                Toast.makeText(SignUpActivity.this, "" + error, Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("name", nameValue);
                params.put("email", emailValue);
                params.put("password", passwordValue);
                params.put("mobile_no", mobileValue);
                //    Log.d("TAG", "Does it assign headers?" + apiParamter());

                return params;
            }


        };

        MyApplication.getInstance().addToRequestQueue(stringRequest);

    }*/


}
