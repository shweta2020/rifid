package com.nusy.rfid.commonMudules;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreference_main {

    Context mContext;
    private static SharedPreference_main sharedPreference_main;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;

    public SharedPreference_main(Context context) {
        mContext = context;
        sharedPreference = context.getSharedPreferences("Shared_Pre", Context.MODE_PRIVATE);
        editor = sharedPreference.edit();
    }

    public static SharedPreference_main getInstance(Context ctx) {
        if (sharedPreference_main == null) {
            sharedPreference_main = new SharedPreference_main(ctx);
            return sharedPreference_main;
        }
        return sharedPreference_main;
    }

    public void removePreference() {
        editor = sharedPreference.edit();
        editor.clear().apply();
    }

    public void setIs_LoggedIn(boolean Is_LoggedIn) {
        editor = sharedPreference.edit();
        editor.putBoolean("Is_LoggedIn", Is_LoggedIn);
        editor.commit();
    }

    public boolean getIs_LoggedIn() {
        return sharedPreference.getBoolean("Is_LoggedIn", false);
    }

    public void setUsername(String username) {
        editor = sharedPreference.edit();
        editor.putString("username", username);
        editor.commit();
    }

    public String getUsername() {
        return sharedPreference.getString("username", "");
    }


    public void setId(int Id) {
        editor = sharedPreference.edit();
        editor.putInt("Id", Id);
        editor.commit();
    }

    public int getId() {
        return sharedPreference.getInt("Id", 0);
    }


    public void setToken(String Token) {
        editor = sharedPreference.edit();
        editor.putString("Token", Token);
        editor.commit();
    }

    public String getToken() {
        return sharedPreference.getString("Token", "");
    }

    public void setDeviceToken(String DeviceToken) {
        editor = sharedPreference.edit();
        editor.putString("Token", DeviceToken);
        editor.apply();
        editor.commit();
    }

    public String getDeviceToken() {
        return sharedPreference.getString("DeviceToken", "");
    }

    public void setname(String name) {
        editor = sharedPreference.edit();
        editor.putString("name", name);
        editor.commit();
    }

    public String getname() {
        return sharedPreference.getString("name", "");
    }

    public void setImage(String Image) {
        editor = sharedPreference.edit();
        editor.putString("Image", Image);
        editor.commit();
    }

    public String getImage() {
        return sharedPreference.getString("Image", "");
    }

    public void setemail(String email) {
        editor = sharedPreference.edit();
        editor.putString("email", email);
        editor.commit();
    }

    public String getemail() {
        return sharedPreference.getString("email", "");
    }

    public void setmobile(String mobile) {
        editor = sharedPreference.edit();
        editor.putString("mobile", mobile);
        editor.commit();
    }

    public String getmobile() {
        return sharedPreference.getString("mobile", "");
    }

    public void setdob(String dob) {
        editor = sharedPreference.edit();
        editor.putString("dob", dob);
        editor.commit();
    }

    public String getdob() {
        return sharedPreference.getString("dob", "");
    }

    public void seteducation(String education) {
        editor = sharedPreference.edit();
        editor.putString("education", education);
        editor.commit();
    }

    public String geteducation() {
        return sharedPreference.getString("education", "");
    }

    public void setaddress(String address) {
        editor = sharedPreference.edit();
        editor.putString("address", address);
        editor.commit();
    }

    public String getaddress() {
        return sharedPreference.getString("address", "");
    }

    public void setInititateSignUp(Boolean inititateSignUp) {
        editor = sharedPreference.edit();
        editor.putBoolean("inititateSignUp", inititateSignUp);
        editor.commit();
    }

    public Boolean getinititateSignUp() {
        return sharedPreference.getBoolean("inititateSignUp", false);
    }

    public void setEvent_id(String Event_id) {
        editor = sharedPreference.edit();
        editor.putString("Event_id", Event_id);
        editor.commit();
    }

    public String getEvent_id() {
        return sharedPreference.getString("Event_id", "");
    }

    // Save Event Info
    public void setEvent_name(String Event_name) {
        editor = sharedPreference.edit();
        editor.putString("Event_name", Event_name);
        editor.commit();
    }
    public String getEvent_name() {
        return sharedPreference.getString("Event_name", "");
    }
    public String getEvent_venue() {
        return sharedPreference.getString("Event_venue", "");
    }

    public void setEvent_venue(String Event_venue) {
        editor = sharedPreference.edit();
        editor.putString("Event_venue", Event_venue);
        editor.commit();
    }

    public String getEvent_time() {
        return sharedPreference.getString("Event_time", "");
    }

    public void setEvent_time(String Event_time) {
        editor = sharedPreference.edit();
        editor.putString("Event_time", Event_time);
        editor.commit();
    }

    public String getEvent_date() {
        return sharedPreference.getString("Event_date", "");
    }

    public void setEvent_date(String Event_date) {
        editor = sharedPreference.edit();
        editor.putString("Event_date", Event_date);
        editor.commit();
    }

    public String getEvent_image() {
        return sharedPreference.getString("Event_image", "");
    }

    public void setEvent_image(String Event_image) {
        editor = sharedPreference.edit();
        editor.putString("Event_image", Event_image);
        editor.commit();
    }

    public String getEvent_amount() {
        return sharedPreference.getString("Event_amount", "");
    }

    public void setEvent_amount(String Event_amount) {
        editor = sharedPreference.edit();
        editor.putString("Event_amount", Event_amount);
        editor.commit();
    }
    public String getfood_amount() {
        return sharedPreference.getString("Food_amount", "");
    }

    public void setfood_amount(String Food_amount) {
        editor = sharedPreference.edit();
        editor.putString("Food_amount", Food_amount);
        editor.commit();
    }

    public String getQty() {
        return sharedPreference.getString("Qty", "");
    }

    public void setQty(String Qty) {
        editor = sharedPreference.edit();
        editor.putString("Qty", Qty);
        editor.commit();
    }

    public String getfoodeventid() {
        return sharedPreference.getString("foodevent", "");
    }

    public void setfoodeventid(String foodevent) {
        editor = sharedPreference.edit();
        editor.putString("foodevent", foodevent);
        editor.commit();
    }

    public String getfoodorderid() {
        return sharedPreference.getString("foodorderid", "");
    }

    public void setfoodorderid(String foodorderid) {
        editor = sharedPreference.edit();
        editor.putString("foodorderid", foodorderid);
        editor.commit();
    }
    public String getticket() {
        return sharedPreference.getString("Ticket", "");
    }

    public void setticket(String Ticket) {
        editor = sharedPreference.edit();
        editor.putString("Ticket", Ticket);
        editor.commit();
    }

    public String getamt() {
        return sharedPreference.getString("amt", "");
    }

    public void setamt(String amt) {
        editor = sharedPreference.edit();
        editor.putString("amt", amt);
        editor.commit();
    }


}
