package com.nusy.rfid.commonMudules;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.nusy.rfid.R;

import java.util.Calendar;

public class Extension {


//    public static void showToastMessage(Context context, String message){
//
//        LayoutInflater inflater = ((com.nusy.rfid.Activity)context).getLayoutInflater();
//        View layout=inflater.inflate(R.layout.custom_toast,(ViewGroup)( ((com.nusy.rfid.Activity) context).findViewById(R.id.relativelayout)));
//
////        View layout = inflater.inflate(R.layout.custom_toast,
////                (ViewGroup) ((com.nusy.rfid.Activity) context).findViewById(R.id.relativelayout));
////        // set a message
//        TextView text = (TextView) layout.findViewById(R.id.toast);
//        text.setText(message);
//
//        // Toast...
//        Toast toast = new Toast(context);
//        toast.setGravity(Gravity.BOTTOM, 0, 0);
//        toast.setDuration(Toast.LENGTH_LONG);
//        toast.setView(layout);
//        toast.show();
//    }
    public static void showtoast(Context context,String message){
        Toast.makeText(context, ""+message, Toast.LENGTH_SHORT).show();
    }

    public static void printLog(String msg){

       Log.e("error",msg);
    }
    public static void printResponse(String msg){
        Log.e("response",msg);
    }



    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void openCelender(Context context, int year, int month, int day, final TextView textView){
        final Calendar c;
        c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        final DatePickerDialog mDatePicker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                textView.setText(new StringBuilder().append(selectedyear).append("-").append(selectedmonth + 1).append("-").append(selectedday));
            }
        }, year, month, day);
        mDatePicker.setTitle("Please select date");
//                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        mDatePicker.show();


    }

    public static void showErrorDialog(Context context, final Dialog dialog){

//        dialog=new Dialog(context);
        dialog.setContentView(R.layout.error_dialog);
        TextView tvOk=dialog.findViewById(R.id.text_ok);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            dialog.dismiss();
            }
        });



    }





}

