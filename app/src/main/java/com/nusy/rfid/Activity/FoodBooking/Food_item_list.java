package com.nusy.rfid.Activity.FoodBooking;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.nusy.rfid.R;
import com.nusy.rfid.adapter.Adapter_food_list;
import com.nusy.rfid.commonMudules.NetworkUtils;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.model.Food.ResponeFoodMenu;
import com.nusy.rfid.retrofit.ApiClient;
import com.nusy.rfid.retrofit.ServiceInterface;
import com.nusy.rfid.utils.DialogsUtils;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusy.rfid.commonMudules.Extension.showtoast;
import static com.nusy.rfid.utils.Constants.Content_Type;

public class Food_item_list extends AppCompatActivity {
    String str_eventId;
    Adapter_food_list Adapter_food;
    SharedPreference_main sharedPreference_main;
    RecyclerView recyclerViewTicketinfo;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_item_list);
        sharedPreference_main = SharedPreference_main.getInstance(Food_item_list.this);
        inti();
        food_booking();

    }


    private void food_booking() {
        progressDialog = DialogsUtils.showProgressDialog(this, "Please wait...");
        HashMap<String, String> map = new HashMap<>();
        map.put("event_id", str_eventId);


        if (NetworkUtils.isConnected(Food_item_list.this)) {
            progressDialog.show();
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeFoodMenu> call = serviceInterface.FoodMenu(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeFoodMenu>() {
                @Override
                public void onResponse(Call<ResponeFoodMenu> call, Response<ResponeFoodMenu> response) {

//                    printLog(response.body().toString());

                    if (response.isSuccessful()) {
                        progressDialog.show();
                        ResponeFoodMenu bean = response.body();
                        // printLog(bean.toString());
                        if (bean.getStatus() == 200) {
                            if (bean.getData() != null) {
                                progressDialog.dismiss();
                                mSwipeRefreshLayout.setRefreshing(false);
                                Adapter_food = new Adapter_food_list(Food_item_list.this, bean.getData());
                                recyclerViewTicketinfo.setLayoutManager(new LinearLayoutManager(Food_item_list.this, LinearLayoutManager.VERTICAL, false));
                                recyclerViewTicketinfo.setItemAnimator(new DefaultItemAnimator());
                                recyclerViewTicketinfo.setAdapter(Adapter_food);
                            }
                        } else {
                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);
                            showtoast(Food_item_list.this, "No data found");
                        }
                    } else {
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        showtoast(Food_item_list.this, "Something is wrong");
                    }
                }

                @Override
                public void onFailure(Call<ResponeFoodMenu> call, Throwable t) {
                    progressDialog.dismiss();
                }


            });
        } else {
            Toast.makeText(Food_item_list.this, "Please Check the Internet Connection", Toast.LENGTH_LONG).show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            }, 2800);
            mSwipeRefreshLayout.setRefreshing(false);

        }
    }

    public void inti() {
        recyclerViewTicketinfo = findViewById(R.id.recyclerTicketinfo);
        str_eventId = getIntent().getStringExtra("event_id");

        sharedPreference_main.setfoodeventid(str_eventId);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_home);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        food_booking();
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                }
        );
    }
}
