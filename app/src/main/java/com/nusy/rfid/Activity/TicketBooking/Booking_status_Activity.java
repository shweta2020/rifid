package com.nusy.rfid.Activity.TicketBooking;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.nusy.rfid.R;
import com.nusy.rfid.adapter.Ticket_info_Adapter;
import com.nusy.rfid.commonMudules.NetworkUtils;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.model.Ticket_booking_info.ResponeTicketBook;
import com.nusy.rfid.retrofit.ApiClient;
import com.nusy.rfid.retrofit.ServiceInterface;
import com.nusy.rfid.utils.DialogsUtils;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusy.rfid.commonMudules.Extension.printLog;
import static com.nusy.rfid.commonMudules.Extension.showtoast;
import static com.nusy.rfid.utils.Constants.Content_Type;

public class Booking_status_Activity extends AppCompatActivity {
    Ticket_info_Adapter ticketAdapterinfo;
    SharedPreference_main sharedPreference_main;
    RecyclerView recyclerViewTicketinfo;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Booking_status_Activity.this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_booking_status_);
        init();
      //  this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ticket_Booked();
/*
        sharedPreference_main = SharedPreference_main.getInstance(this);
        recyclerViewTicketinfo = findViewById(R.id.recyclerTicketinfo);
        sharedPreference_main = SharedPreference_main.getInstance(this);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_home);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        ticket_Booked();
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                }
        );
*/
    }


    private void ticket_Booked() {
        progressDialog = DialogsUtils.showProgressDialog(this, "Please wait...");
        HashMap<String, String> map = new HashMap<>();

        map.put("user_id", String.valueOf(sharedPreference_main.getId()));
        if (NetworkUtils.isConnected(Booking_status_Activity.this)) {
            progressDialog.show();
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeTicketBook> call = serviceInterface.getticketinfo(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeTicketBook>() {
                @Override
                public void onResponse(Call<ResponeTicketBook> call, Response<ResponeTicketBook> response) {

//                    printLog(response.body().toString());

                    if (response.isSuccessful()) {
                        progressDialog.show();
                                ResponeTicketBook bean = response.body();
                                printLog(bean.toString());


                                if (bean.getStatus() == 200) {
                                    if (bean.getData() != null) {
                                        progressDialog.dismiss();

                                mSwipeRefreshLayout.setRefreshing(false);
                                ticketAdapterinfo = new Ticket_info_Adapter(Booking_status_Activity.this, bean.getData());
                                recyclerViewTicketinfo.setLayoutManager(new LinearLayoutManager(Booking_status_Activity.this, LinearLayoutManager.VERTICAL, false));
                                recyclerViewTicketinfo.setItemAnimator(new DefaultItemAnimator());
                                recyclerViewTicketinfo.setAdapter(ticketAdapterinfo);
                            }else {
                                        progressDialog.dismiss();

                                        showtoast(Booking_status_Activity.this, "No data found");
                                    }
                        } else {
                                    progressDialog.dismiss();

                                    showtoast(Booking_status_Activity.this, "No data found");
                        }
                    } else {
                        progressDialog.dismiss();

                        mSwipeRefreshLayout.setRefreshing(false);
                        showtoast(Booking_status_Activity.this, "Something is wrong");

                    }
                }

                @Override
                public void onFailure(Call<ResponeTicketBook> call, Throwable t) {
                    progressDialog.dismiss();

                    Log.e("error", t.getMessage());
                }
            });
        } else {
            progressDialog.dismiss();

            Toast.makeText(Booking_status_Activity.this, "Please Check the Internet Connection", Toast.LENGTH_LONG).show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            }, 2800);
            mSwipeRefreshLayout.setRefreshing(false);


        }
    }
    private void init(){
        sharedPreference_main = SharedPreference_main.getInstance(this);
        recyclerViewTicketinfo = findViewById(R.id.recyclerTicketinfo);
        sharedPreference_main = SharedPreference_main.getInstance(this);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_home);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        ticket_Booked();
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                }
        );

    }
}
