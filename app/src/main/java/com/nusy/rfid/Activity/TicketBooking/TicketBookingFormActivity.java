package com.nusy.rfid.Activity.TicketBooking;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.nusy.rfid.R;
import com.nusy.rfid.adapter.CustomArrayAdapter;
import com.nusy.rfid.commonMudules.NetworkUtils;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.model.Parking.ResponeParking;
import com.nusy.rfid.retrofit.ApiClient;
import com.nusy.rfid.retrofit.ServiceInterface;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusy.rfid.commonMudules.Extension.printLog;
import static com.nusy.rfid.commonMudules.Extension.showtoast;
import static com.nusy.rfid.utils.Constants.Content_Type;

public class TicketBookingFormActivity extends AppCompatActivity {
    EditText et_name, et_email, et_mobile, et_address, et_pin;
    Button bt_submit;
    String str_name, str_email, str_pin, str_mobile, str_address;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    TextView tx_parking;
    Spinner sp_spinner;
    String str_fare, idd;
    SharedPreference_main sharedPreference_main;
    private ArrayList<ResponeParking> goodModelArrayList;
    private ArrayList<String> playerNames = new ArrayList<String>();
    private ArrayList<String> name = new ArrayList<String>();
    private ArrayList<String> id = new ArrayList<String>();
    private ArrayList<String> amount = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_booking_form);
        et_address = findViewById(R.id.edt_address);
        et_email = findViewById(R.id.edt_email);
        et_mobile = findViewById(R.id.edt_mobile);
        et_name = findViewById(R.id.edt_name);
        et_pin = findViewById(R.id.edt_pin);
        bt_submit = findViewById(R.id.btn_submit);
        tx_parking = findViewById(R.id.txt_parking);
        sp_spinner = findViewById(R.id.spn_spinner);
        String str_amount;
        sharedPreference_main = SharedPreference_main.getInstance(this);
        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_name = et_name.getText().toString();
                str_address = et_address.getText().toString();
                str_email = et_email.getText().toString();
                str_pin = et_pin.getText().toString();
                str_mobile = et_mobile.getText().toString();
                checkvalid();
            }
        });
        getSpinnerData();
    }

    private void checkvalid() {

        if (et_name.getText().toString().isEmpty()) {
            et_name.setError("Field can't be empty");
            //  Toast.makeText(this, "enter Name:", Toast.LENGTH_SHORT).show();
        } else if (!et_email.getText().toString().matches(emailPattern)) {
            //Toast.makeText(this, "enter Valid Email:", Toast.LENGTH_SHORT).show();
            et_email.setError("Field can't be empty");
        } else if (et_mobile.getText().toString().length() < 10) {
            et_mobile.setError("Field can't be empty");
            //Toast.makeText(this, "Please enter a valid Mobile Number", Toast.LENGTH_SHORT).show();

        } else {
            save();

        }

    }


    private void getSpinnerData() {
        HashMap<String, String> map = new HashMap<>();
        map.put("event_id", sharedPreference_main.getEvent_id());

        if (NetworkUtils.isConnected(TicketBookingFormActivity.this)) {

            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeParking> call = serviceInterface.getspinnerlist(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeParking>() {
                @Override
                public void onResponse(Call<ResponeParking> call, Response<ResponeParking> response) {

                    if (response.isSuccessful()) {

                        final ResponeParking bean = response.body();
                        printLog(bean.toString());
                        if (bean.getStatus() == 200) {
                            if (bean.getData() != null) {

                                for (int i = 0; i < bean.getData().size(); i++) {
                                    name.add(bean.getData().get(i).getName());
                                    id.add(bean.getData().get(i).getParkingPriceId());
                                    amount.add(bean.getData().get(i).getAmount());
                                }

                                CustomArrayAdapter adapter = new CustomArrayAdapter(TicketBookingFormActivity.this,
                                        R.layout.spinner_data, bean.getData());
                                sp_spinner.setAdapter(adapter);
                                sp_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                                        // TODO Auto-generated method stub

                                        //String id = name.get(position);
                                        for (int i = 0; i < id.size(); i++) {
                                            // String id = bean.getData().get(i).getParkingPriceId();
                                            idd = id.get(position);
                                            Log.e("Id----->", idd.toString());
                                        }
                                        for (int i = 0; i < amount.size(); i++) {
                                            String str_amount = amount.get(position);
                                            Log.e("amount", str_amount.toString());
                                            str_fare = str_amount;
                                        }

                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> arg0) {
                                        // TODO Auto-generated method stub
                                    }
                                });
                                //sp_spinner.setAdapter(ResponeParking.Datap.class.getName().toString());
                                /*sp_spinner.setAdapter(new ArrayAdapter<ResponeParking.Datap>
                                (TicketBookingFormActivity.this, simple_spinner_item,bean.getData()));*/
                            }
                        } else {
                            showtoast(TicketBookingFormActivity.this, "No data found");
                        }
                    } else {
                        showtoast(TicketBookingFormActivity.this, "Something is wrong");

                    }
                }

                @Override
                public void onFailure(Call<ResponeParking> call, Throwable t) {


                }
            });
        } else {
            Toast.makeText(TicketBookingFormActivity.this, "Please Check the Internet Connection", Toast.LENGTH_LONG).show();


        }
    }

    private void save() {
        Intent intent = new Intent(TicketBookingFormActivity.this, Confirm_Booking_Activity.class);
        intent.putExtra("name", str_name);
        intent.putExtra("address", str_address);
        intent.putExtra("mobile", str_mobile);
        intent.putExtra("email", str_email);
        intent.putExtra("pin", str_pin);
        intent.putExtra("amount", str_fare);
        intent.putExtra("id", idd);
        startActivity(intent);

    }
}
