package com.nusy.rfid.Activity;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.nusy.rfid.Activity.FoodStatus.MyFoodBookingStatus;
import com.nusy.rfid.Activity.TicketBooking.Booking_status_Activity;
import com.nusy.rfid.R;
import com.nusy.rfid.commonMudules.Extension;
import com.nusy.rfid.commonMudules.NetworkUtils;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.fragment.Cart;
import com.nusy.rfid.fragment.ChangePassword;
import com.nusy.rfid.fragment.EventHome;
import com.nusy.rfid.retrofit.ApiClient;
import com.nusy.rfid.retrofit.ServiceInterface;
import com.nusys.rfid.model.ResponseModel;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusy.rfid.commonMudules.Extension.printLog;
import static com.nusy.rfid.commonMudules.Extension.showtoast;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    //    String tokenValue,id,name,image,emmm,
    String totalitem = "0";
    DrawerLayout drawer;
    Button eventButton;
    CircleImageView profileImage;
    private Fragment fragment = null;
    private FragmentManager fragmentManager;
    private NavigationView navigationView;
    TextView nameProfile, emailprofile;
    String cartUrl = "http://13.233.162.24/rifid/api/web_api/count_cart";
    TextView inbox, textCartItemCount;
    int mCartItemCount = 0;
    SharedPreference_main sharedPreference_main;
    Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //    toolbar.setLogo(R.drawable.logo);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        dialog = new Dialog(this);

        Intent intent = getIntent();
//        tokenValue= intent.getStringExtra("tokenValue");
//        name= intent.getStringExtra("Name");
//        id= intent.getStringExtra("id");
//        image= intent.getStringExtra("Image");
//        emmm= intent.getStringExtra("email");


//        Log.e("mainActivity","tkvl"+tokenValue);
//        Log.e("mainActivity","id"+id);
//        Log.e("mainActivity","name"+name);


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        fragmentManager = getSupportFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragment = new EventHome();
        fragmentTransaction.replace(R.id.contentmain, fragment);
        fragmentTransaction.commit();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        /*View view=navigationView.inflateHeaderView(R.layout.nav_header_main);*/
        nameProfile = (TextView) header.findViewById(R.id.profileName);
        emailprofile = (TextView) header.findViewById(R.id.emailProfile);
        profileImage = (CircleImageView) header.findViewById(R.id.profile_image);

        init();

        //cartApi();


    }

    private void cartApi() {

        if (NetworkUtils.isConnected(this)) {

            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponseModel> call = serviceInterface.getCart(sharedPreference_main.getId());
            call.enqueue(new Callback<ResponseModel>() {
                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

//                    printLog(response.body().toString());


                    if (response.isSuccessful()) {

                        ResponseModel bean = response.body();

                        printLog(bean.toString());
                        if (bean.getStatus() == 200) {

//                                totalitem= String.valueOf(bean.getData().get(0).getTotal_items());
                            setupBadge(String.valueOf(bean.getData().get(0).getTotal_items()));


                        }


                    } else {
                        showtoast(MainActivity.this, "Something is wrong");


                    }
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    printLog(t.getMessage());

                }
            });
        } else {
            Extension.showErrorDialog(this, dialog);
        }

    }

    private void init() {
        sharedPreference_main = SharedPreference_main.getInstance(this);
        nameProfile.setText(sharedPreference_main.getname());
        emailprofile.setText(sharedPreference_main.getemail());
        Glide.with(this)
                .load(sharedPreference_main.getImage())
                .into(profileImage);
    }

    @Override
    public void onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        final MenuItem menuItem = menu.findItem(R.id.cart);
        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });

        return true;
    }

    private void setupBadge(String totalitem) {
        mCartItemCount = Integer.valueOf(totalitem);

        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the ScheduleService/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.cart) {

            fragment = new Cart();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.contentmain, fragment);
            transaction.commit();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            fragment = new EventHome();
        } else if (id == R.id.nav_profile) {

        } else if (id == R.id.nav_service) {

        } else if (id == R.id.nav_food) {

            startActivity(new Intent(MainActivity.this, MyFoodBookingStatus.class));
        } else if (id == R.id.nav_event) {
            Intent intent = new Intent(MainActivity.this, Booking_status_Activity.class);
            startActivity(intent);

        } else if (id == R.id.change_password) {

            fragment = new ChangePassword();
        } else if (id == R.id.nav_login) {

            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Logout?")
                    .setMessage("Are you sure you want to Logout?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {

                            sharedPreference_main.removePreference();
                            sharedPreference_main.setIs_LoggedIn(false);

                            SharedPreferences SM = getSharedPreferences("userrecord", 0);
                            SharedPreferences.Editor edit = SM.edit();
                            edit.putBoolean("userlogin", false);
                            edit.commit();
                            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();

                        }
                    }).create().show();
            drawer.closeDrawer(GravityCompat.START);
        }
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.contentmain, fragment);
        transaction.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;

        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    public String getMyData() {
        return sharedPreference_main.getToken();
    }

    public String getIDData() {
        return String.valueOf(sharedPreference_main.getId());
    }


//    private void cartApi() {
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, cartUrl,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//
//                        try {
//
//                            JSONObject jsonObject = new JSONObject(response);
//                            Log.e("TAG", "LEAD_jsonObject" + jsonObject);
//
//
//                            JSONArray jsonArray =  jsonObject.getJSONArray("data");
//
//                            for (int i = 0; i < jsonArray.length(); i++) {
//
//                                JSONObject jsonItemObject = jsonArray.getJSONObject(i);
//
//                                    totalitem =   jsonItemObject.getString("total_items");
//
//                                Log.e("fffuccccck","succkckck"+jsonItemObject.getString("total_items"));
//
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            Log.e("JSON Parser", "Error parsing data [" + e.getMessage() + "] " + response);
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e("TAG", "onErrorResponse: " + String.valueOf(error));
//                Toast.makeText(MainActivity.this, ""+error, Toast.LENGTH_SHORT).show();
//
//            }
//        }) {
//            @Override
//            public Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//
//                params.put("user_id", String.valueOf(sharedPreference_main.getId()));
//
//                //    Log.d("TAG", "Does it assign headers?" + apiParamter());
//
//                return params;
//            }
//
//       /*    @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//
//                //This appears in the log
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("", apiParamter());
//                Log.d("TAG", "Does it assign headers?" + apiParamter());
//                return headers;
//            }  */
//        };
//
//        MyApplication.getInstance().addToRequestQueue(stringRequest);
//
//    }


}
