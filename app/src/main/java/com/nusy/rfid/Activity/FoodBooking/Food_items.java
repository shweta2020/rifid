package com.nusy.rfid.Activity.FoodBooking;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.nusy.rfid.Activity.MainActivity;
import com.nusy.rfid.R;
import com.nusy.rfid.commonMudules.NetworkUtils;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.model.Food.ResponeFoodPayment;
import com.nusy.rfid.retrofit.ApiClient;
import com.nusy.rfid.retrofit.ServiceInterface;
import com.nusy.rfid.utils.DialogsUtils;
import com.nusy.rfid.utils.PaypalConfig;
import com.nusys.rfid.model.Food.singleFood;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusy.rfid.commonMudules.Extension.printLog;
import static com.nusy.rfid.commonMudules.Extension.showtoast;
import static com.nusy.rfid.utils.Constants.Content_Type;

public class Food_items extends AppCompatActivity {
    SharedPreference_main sharedPreference_main;
    String str_food_Id, str_s_amount, Quantity, str_final, s;
    Button bt_food_pay;
    TextView tx_food, tx_price, tx_finalamount;
    ImageView imageView;
    public int price, totalAmount;
    private String[] pickerVals;
    NumberPicker picker1;
    Dialog dialog;
    ImageView imb_ozow,imb_paypal;
    ProgressDialog progressDialog;
    //public NumberPicker numberPicker;
    String str_qty, str_transationId, paymentDetails;
    //NumberPicker numberPicker;
    public static final int PAYPAL_REQUEST_CODE = 123;
    TextView tx_pay_amount;
    int pay_amount;
    // SwipeRefreshLayout mSwipeRefreshLayout;
    private static PayPalConfiguration config = new PayPalConfiguration() // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(PaypalConfig.PAYPAL_CLIENT_ID)
            .merchantName("TechNxt Code Labs")
            .merchantPrivacyPolicyUri(
                    Uri.parse("https://www.paypal.com/webapps/mpp/ua/privacy-full"))
            .merchantUserAgreementUri(
                    Uri.parse("https://www.paypal.com/webapps/mpp/ua/useragreement-full"));  // or live (ENVIRONMENT_PRODUCTION)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_food_items);
        //  numberPicker = (NumberPicker) findViewById(R.id.number_picker);
        bt_food_pay = findViewById(R.id.btn_food_pay);
        tx_food = findViewById(R.id.txt_food_name_s);
        tx_price = findViewById(R.id.txt_food_price_s);
        tx_finalamount = findViewById(R.id.finalamount);
        imageView = findViewById(R.id.img_food);
        picker1 = findViewById(R.id.numberPicker);
        str_food_Id = getIntent().getStringExtra("food_id");
        picker1.setMinValue(0);
        picker1.setMaxValue(9);
        pickerVals = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        picker1.setDisplayedValues(pickerVals);
        // numberPicker.setWrapSelectorWheel(true);
        sharedPreference_main = SharedPreference_main.getInstance(Food_items.this);
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);


        Log.e("food_id", str_food_Id);
        dialog = new Dialog(Food_items.this);
        // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.payment_dialog);
     /*   bt_food_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // startActivity(new Intent(Food_items.this, Food_payment.class));
              //  getPayment();
                dialog.show();
            }
        });*/
        imb_ozow = dialog.findViewById(R.id.ozow);
        imb_paypal = dialog.findViewById(R.id.paypal);
        imb_paypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPayment();
            }
        });
        imb_ozow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Coming Soon", Toast.LENGTH_LONG).show();
            }
        });
        food_booking();
      /*  mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_home);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        food_booking();
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                }
        );*/
    }

    private void food_booking() {
        progressDialog = DialogsUtils.showProgressDialog(this, "Please wait...");
        HashMap<String, String> map = new HashMap<>();
        map.put("food_id", str_food_Id);

        if (NetworkUtils.isConnected(Food_items.this)) {
            progressDialog.show();
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<singleFood> call = serviceInterface.SingleFood(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<singleFood>() {
                @Override
                public void onResponse(Call<singleFood> call, Response<singleFood> response) {

//                    printLog(response.body().toString());

                    if (response.isSuccessful()) {
                        progressDialog.show();
                        final singleFood bean = response.body();
                        // printLog(bean.toString());
                        if (bean.getStatus() == 200) {
                            if (bean.getData() != null) {
                                progressDialog.dismiss();
                                //  mSwipeRefreshLayout.setRefreshing(false);
                                tx_food.setText(bean.getData().get(0).getFood_item());
                                tx_price.setText(bean.getData().get(0).getAmount());
                                Picasso.with(getApplicationContext()).load(bean.getData().get(0).getImage()).into(imageView);
                                //tx_finalamount.setText(bean.getData().get(0).getAmount());

                                picker1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                                    @Override
                                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                        int valuePicker1 = picker1.getValue();
                                        price = Integer.valueOf(bean.getData().get(0).getAmount());
                                        s = pickerVals[valuePicker1];
                                        totalAmount = Integer.valueOf(s) * price;
                                        //str_final = String.valueOf(totalAmount);
                                        tx_finalamount.setText(String.valueOf(totalAmount));
                                        if (totalAmount>1){
                                            bt_food_pay.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    // startActivity(new Intent(Food_items.this, Food_payment.class));
                                                    //  getPayment();
                                                    dialog.show();
                                                }
                                            });
                                        }else {
                                            bt_food_pay.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    // startActivity(new Intent(Food_items.this, Food_payment.class));
                                                    //  getPayment();
                                                    Toast.makeText(getApplicationContext(), "Please Add the Amount", Toast.LENGTH_LONG).show();
                                                }
                                            });
                                            Toast.makeText(getApplicationContext(), "Please Add the Amount", Toast.LENGTH_LONG).show();
                                        }

                                        Log.d("picker value", String.valueOf(totalAmount));
                                        //  Log.d("picker value", String.valueOf(totalAmount));
                                    }
                                });

                                NumberPicker.OnValueChangeListener onValueChangeListener = new NumberPicker.OnValueChangeListener() {
                                    @Override
                                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                        // Toast.makeText(Food_items.this, picker.newVal, Toast.LENGTH_LONG).show();
                                        //Log.e("picker", String.valueOf(picker.getValue()));
                                    }
                                };


                            }
                        } else {
                            progressDialog.dismiss();
                            //   mSwipeRefreshLayout.setRefreshing(false);
                            showtoast(Food_items.this, "No data found");
                        }
                    } else {
                        progressDialog.dismiss();
                        // mSwipeRefreshLayout.setRefreshing(false);
                        showtoast(Food_items.this, "Something is wrong");
                    }
                }

                @Override
                public void onFailure(Call<singleFood> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(Food_items.this, "Please Check the Internet Connection", Toast.LENGTH_LONG).show();
        }

    }

    private void getPayment() {

        //Creating a paypalpayment
        PayPalPayment payment = new PayPalPayment(new
                BigDecimal(totalAmount/*Integer.parseInt(tx_finalamount.getText().toString())*/), "USD", "Test", PayPalPayment.PAYMENT_INTENT_SALE);

        //Creating Paypal Payment activity intent
        Intent intent = new Intent(Food_items.this, PaymentActivity.class);
        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        startActivityForResult(intent, PAYPAL_REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAYPAL_REQUEST_CODE) {
            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                Log.d("CONFIRM", String.valueOf(confirm));
                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        paymentDetails = confirm.toJSONObject().toString(4);
                        Log.d("paymentExample", paymentDetails);
                        Log.i("paymentExample", paymentDetails);
                        Log.e("Payment ID", paymentDetails);
                        Log.d("Pay Confirm : ", String.valueOf(confirm.getPayment().toJSONObject()));
//                        Starting a new activity for the payment details and status to show
                        try {
                            JSONObject jsonDetails = new JSONObject(paymentDetails);

                            //Displaying payment details
                            showDetails(jsonDetails.getJSONObject("response"));
                            //str_transationId = jsonDetails.getString("id");
                        } catch (JSONException e) {
                            Toast.makeText(Food_items.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        food_payment();
                        //    startActivity(new Intent(Food_payment.this, Home.class));
                      /*  startActivity(new Intent(Confirm_Booking_Activity.this, PaymentDetails.class)
                                .putExtra("PaymentDetails", paymentDetails));*/

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred : ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    private void showDetails(JSONObject jsonDetails) throws JSONException {
        //Views
        // TextView textViewId = (TextView) findViewById(R.id.paymentId);
        // TextView textViewStatus = (TextView) findViewById(R.id.paymentStatus);

        //Showing the paypal payment details and status from json object

        str_transationId = jsonDetails.getString("id");
        //  textViewId.setText(jsonDetails.getString("id"));
        //  textViewStatus.setText(jsonDetails.getString("state"));


    }

    private void food_payment() {
        HashMap<String, String> map = new HashMap<>();

       /* "order_id":"59",
                "event_id":"1",
                "food_id":"1",
                "visitor_id":"1",
                "quantity":"1",
                "payment_status":"1",
                "payment_type":"online",
                "total_amount":"100",
                "transaction_id":"1000325562"*/


        map.put("order_id", sharedPreference_main.getfoodorderid());
        map.put("event_id", sharedPreference_main.getfoodeventid());
        map.put("food_id", str_food_Id);
        map.put("visitor_id", String.valueOf(sharedPreference_main.getId()));
        map.put("quantity", s);
        map.put("payment_status", "1");
        map.put("payment_type", "online");
        map.put("total_amount", String.valueOf(totalAmount));
        map.put("transaction_id", str_transationId);
        if (NetworkUtils.isConnected(Food_items.this)) {

            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeFoodPayment> call = serviceInterface.Food_payment(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeFoodPayment>() {
                @Override
                public void onResponse(Call<ResponeFoodPayment> call, Response<ResponeFoodPayment> response) {

//                    printLog(response.body().toString());

                    if (response.isSuccessful()) {

                        ResponeFoodPayment bean = response.body();
                        printLog(bean.toString());


                        if (bean.getStatus() == 200) {
                            /* if (bean.getData() != null) {*/
                            showtoast(Food_items.this, "Booking Successfully");
                            Intent intent = new Intent(Food_items.this, MainActivity.class);
                            startActivity(intent);
                            // }
                        } else {

                            showtoast(Food_items.this, "No data found");
                        }
                    } else {
                        showtoast(Food_items.this, "Something is wrong");
                    }
                }

                @Override
                public void onFailure(Call<ResponeFoodPayment> call, Throwable t) {
                    Log.e("error", t.getMessage());
                }
            });
        } else {
            Toast.makeText(Food_items.this, "Please Check the Internet Connection", Toast.LENGTH_LONG).show();

        /*if (NetworkUtils.isConnected(Food_items.this)) {

            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeFoodPayment> call = serviceInterface.Food_payment(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeFoodPayment>() {
                @Override
                public void onResponse(Call<ResponeFoodPayment> call, Response<ResponeFoodPayment> response) {

//                    printLog(response.body().toString());

                    if (response.isSuccessful()) {

                        final ResponeFoodPayment bean = response.body();
                        // printLog(bean.toString());
                        if (bean.getStatus() == 200) {
                            if (bean.getData() != null) {
                                startActivity(new Intent(Food_items.this, Home.class));
                            }
                        } else {
                            showtoast(Food_items.this, "No data found");
                        }
                    } else {
                        showtoast(Food_items.this, "Something is wrong");
                    }
                }

                @Override
                public void onFailure(Call<ResponeFoodPayment> call, Throwable t) {

                }
            });
        } else {*/

        }

    }
}
