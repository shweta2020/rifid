package com.nusy.rfid.Activity.TicketBooking;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.nusy.rfid.R;
import com.nusy.rfid.adapter.Adapter_QR;
import com.nusy.rfid.commonMudules.NetworkUtils;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.model.Ticket_booking_info.ResponeQRCode;
import com.nusy.rfid.retrofit.ApiClient;
import com.nusy.rfid.retrofit.ServiceInterface;
import com.nusy.rfid.utils.DialogsUtils;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusy.rfid.commonMudules.Extension.showtoast;
import static com.nusy.rfid.utils.Constants.Content_Type;

public class Show_QR_codeActivity extends AppCompatActivity {
    String str_eventId, str_eventDate, str_orderId;
    int ticket;
    SharedPreference_main sharedPreference_main;
    RecyclerView recyclerViewTicketinfo;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ImageView next;
    Adapter_QR Adapter_qr;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show__qr_code);
        next = findViewById(R.id.next);
        sharedPreference_main = SharedPreference_main.getInstance(Show_QR_codeActivity.this);
        str_eventId = getIntent().getStringExtra("event_id");
        str_eventDate = getIntent().getStringExtra("event_date");
        str_orderId = getIntent().getStringExtra("order_id");
        ticket = Integer.parseInt(getIntent().getStringExtra("ticket"));
        if (ticket > 1) {
            next.setVisibility(View.VISIBLE);
        }
        sharedPreference_main.setfoodeventid(str_eventId);
        recyclerViewTicketinfo = findViewById(R.id.recyclerTicketinfo);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_home);
        showQrCode();
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        showQrCode();
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                }
        );
    }

    private void showQrCode() {
        progressDialog = DialogsUtils.showProgressDialog(this, "Please wait...");
        HashMap<String, String> map = new HashMap<>();
        map.put("event_id", str_eventId);
        map.put("order_id", str_orderId);
        map.put("event_date", str_eventDate);

        if (NetworkUtils.isConnected(Show_QR_codeActivity.this)) {
            progressDialog.show();
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeQRCode> call = serviceInterface.MyQRCode(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeQRCode>() {
                @Override
                public void onResponse(Call<ResponeQRCode> call, Response<ResponeQRCode> response) {

//                    printLog(response.body().toString());

                    if (response.isSuccessful()) {
                        progressDialog.show();
                        ResponeQRCode bean = response.body();
                        // printLog(bean.toString());
                        if (bean.getStatus() == 200) {
                            if (bean.getData() != null) {
                                progressDialog.dismiss();
                                mSwipeRefreshLayout.setRefreshing(false);
                                Adapter_qr = new Adapter_QR(Show_QR_codeActivity.this, bean.getData());
                                recyclerViewTicketinfo.setLayoutManager(new LinearLayoutManager(Show_QR_codeActivity.this, LinearLayoutManager.HORIZONTAL, true));
                                //  recyclerViewTicketinfo.setLayoutManager(new GridLayoutManager(getBaseContext(), 2));
                                //  recyclerViewTicketinfo.setLayoutManager(new LinearLayoutManager(Show_QR_codeActivity.this, LinearLayoutManager.VERTICAL, false));
                                recyclerViewTicketinfo.setItemAnimator(new DefaultItemAnimator());
                                recyclerViewTicketinfo.setAdapter(Adapter_qr);
                            }else {
                                progressDialog.dismiss();
                                mSwipeRefreshLayout.setRefreshing(false);
                                showtoast(Show_QR_codeActivity.this, "No data found");
                            }
                        } else {
                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);
                            showtoast(Show_QR_codeActivity.this, "No data found");
                        }
                    } else {
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        showtoast(Show_QR_codeActivity.this, "Something is wrong");
                    }
                }

                @Override
                public void onFailure(Call<ResponeQRCode> call, Throwable t) {
                    progressDialog.dismiss();
                }


            });
        } else {
            Toast.makeText(Show_QR_codeActivity.this, "Please Check the Internet Connection", Toast.LENGTH_LONG).show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            }, 2800);
            mSwipeRefreshLayout.setRefreshing(false);

        }
    }
}
