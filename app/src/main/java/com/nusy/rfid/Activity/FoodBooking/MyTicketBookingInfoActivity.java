package com.nusy.rfid.Activity.FoodBooking;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.nusy.rfid.R;
import com.nusy.rfid.adapter.Adapter_my_ticket_booking;
import com.nusy.rfid.commonMudules.NetworkUtils;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.model.Response_my_booking.ResponeBook;
import com.nusy.rfid.retrofit.ApiClient;
import com.nusy.rfid.retrofit.ServiceInterface;
import com.nusy.rfid.utils.DialogsUtils;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusy.rfid.commonMudules.Extension.printLog;
import static com.nusy.rfid.commonMudules.Extension.showtoast;
import static com.nusy.rfid.utils.Constants.Content_Type;

public class MyTicketBookingInfoActivity extends AppCompatActivity {
    SharedPreference_main sharedPreference_main;
    RecyclerView recyclerViewTicketinfo;
    TextView tx_name, tx_venue, tx_time, tx_amount, tx_parking, tx_qty;
    String order_id;
    ProgressDialog progressDialog;
    Adapter_my_ticket_booking ticketAdapterinfo;
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_ticket_booking_info);
        inti();
        ticket_Booked_info();
        /*tx_qty = findViewById(R.id.txt_qty);
        tx_amount = findViewById(R.id.txt_amount);
        tx_name = findViewById(R.id.txt_name_i);
        tx_parking = findViewById(R.id.txt_parking);
        tx_time = findViewById(R.id.txt_time);
        tx_venue = findViewById(R.id.txt_venue);*/
       /* sharedPreference_main = SharedPreference_main.getInstance(this);
        recyclerViewTicketinfo = findViewById(R.id.recyclerTicketinfo);

        order_id = getIntent().getStringExtra("order_id");
        sharedPreference_main.setfoodorderid(order_id);

        Log.e("order_id", sharedPreference_main.getfoodorderid());

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_home);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        ticket_Booked_info();
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                }
        );*/
    }

    private void ticket_Booked_info() {
        progressDialog = DialogsUtils.showProgressDialog(this, "Please wait...");
        HashMap<String, String> map = new HashMap<>();
        map.put("order_id", order_id);

        if (NetworkUtils.isConnected(MyTicketBookingInfoActivity.this)) {
            progressDialog.show();
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeBook> call = serviceInterface.ticketinfo(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeBook>() {
                @Override
                public void onResponse(Call<ResponeBook> call, Response<ResponeBook> response) {

//                    printLog(response.body().toString());

                    if (response.isSuccessful()) {
                        progressDialog.show();
                        ResponeBook bean = response.body();
                        printLog(bean.toString());
                        if (bean.getStatus() == 200) {
                            if (bean.getData() != null) {
                                progressDialog.dismiss();
                                mSwipeRefreshLayout.setRefreshing(false);
                                ticketAdapterinfo = new Adapter_my_ticket_booking(MyTicketBookingInfoActivity.this, bean.getData());
                                recyclerViewTicketinfo.setLayoutManager(new LinearLayoutManager(MyTicketBookingInfoActivity.this, LinearLayoutManager.VERTICAL, false));
                                recyclerViewTicketinfo.setItemAnimator(new DefaultItemAnimator());
                                recyclerViewTicketinfo.setAdapter(ticketAdapterinfo);
                            } else {
                                progressDialog.dismiss();
                                mSwipeRefreshLayout.setRefreshing(false);
                                showtoast(MyTicketBookingInfoActivity.this, "No data found");
                            }
                        } else {
                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);
                            showtoast(MyTicketBookingInfoActivity.this, "No data found");
                        }
                    } else {
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        showtoast(MyTicketBookingInfoActivity.this, "Something is wrong");
                    }
                }

                @Override
                public void onFailure(Call<ResponeBook> call, Throwable t) {
                    Log.e("error", t.getMessage());
                    progressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(MyTicketBookingInfoActivity.this, "Please Check the Internet Connection", Toast.LENGTH_LONG).show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            }, 2800);
            mSwipeRefreshLayout.setRefreshing(false);

        }
    }

    public void inti() {
        sharedPreference_main = SharedPreference_main.getInstance(this);
        recyclerViewTicketinfo = findViewById(R.id.recyclerTicketinfo);

        order_id = getIntent().getStringExtra("order_id");
        sharedPreference_main.setfoodorderid(order_id);

        Log.e("order_id", sharedPreference_main.getfoodorderid());

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_home);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        ticket_Booked_info();
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                }
        );
    }
}
