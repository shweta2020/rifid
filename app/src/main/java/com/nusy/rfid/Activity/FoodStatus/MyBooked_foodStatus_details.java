package com.nusy.rfid.Activity.FoodStatus;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.nusy.rfid.R;
import com.nusy.rfid.commonMudules.NetworkUtils;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.model.Food.ResponeFoodItemStatus;
import com.nusy.rfid.retrofit.ApiClient;
import com.nusy.rfid.retrofit.ServiceInterface;
import com.nusy.rfid.utils.DialogsUtils;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusy.rfid.commonMudules.Extension.printLog;
import static com.nusy.rfid.commonMudules.Extension.showtoast;
import static com.nusy.rfid.utils.Constants.Content_Type;

public class MyBooked_foodStatus_details extends AppCompatActivity {
    SharedPreference_main sharedPreference_main;
    String str_food_Id;
    TextView tx_foodname, tx_qty, tx_amount, tx_event_name, tx_event_venue, tx_event_address, tx_time, tx_remark, tx_status;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booked_food_status_details);
        inti();
        getMyFoodstatusdetails();
       /* sharedPreference_main = SharedPreference_main.getInstance(this);
        str_food_Id = getIntent().getStringExtra("food_id");
        Log.e("food Id", str_food_Id);
        tx_foodname = findViewById(R.id.txt_food_name_f);
        tx_qty = findViewById(R.id.txt_food_qty);
        tx_amount = findViewById(R.id.txt_paidamount);
        tx_event_name = findViewById(R.id.txt_event_name_f);
        tx_event_venue = findViewById(R.id.txt_event_venue_f);
        tx_event_address = findViewById(R.id.txt_Address);
        tx_time = findViewById(R.id.txt_time_f);
        tx_remark = findViewById(R.id.txt_remark);
        tx_status = findViewById(R.id.txt_status);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_home);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getMyFoodstatusdetails();
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                }
        );*/
    }

    private void getMyFoodstatusdetails() {
        progressDialog = DialogsUtils.showProgressDialog(this, "Please wait...");
        HashMap<String, String> map = new HashMap<>();

        map.put("id", str_food_Id);


        if (NetworkUtils.isConnected(MyBooked_foodStatus_details.this)) {
            progressDialog.show();
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeFoodItemStatus> call = serviceInterface.MyFoodStatusDetails(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeFoodItemStatus>() {
                @Override
                public void onResponse(Call<ResponeFoodItemStatus> call, Response<ResponeFoodItemStatus> response) {

//                    printLog(response.body().toString());

                    if (response.isSuccessful()) {
                        progressDialog.show();
                        ResponeFoodItemStatus bean = response.body();
                        printLog(bean.toString());


                        if (bean.getStatus() == 200) {
                            if (bean.getData() != null) {
                                progressDialog.dismiss();
                                mSwipeRefreshLayout.setRefreshing(false);

                                tx_amount.setText(bean.getData().get(0).getTotalAmount());
                                tx_event_address.setText(bean.getData().get(0).getEventAddress());
                                tx_event_name.setText(bean.getData().get(0).getEventName());
                                tx_event_venue.setText(bean.getData().get(0).getEventVenue());
                                tx_foodname.setText(bean.getData().get(0).getFoodName());
                                tx_time.setText(bean.getData().get(0).getTime());
                                tx_qty.setText(bean.getData().get(0).getQuantity());
                                if (bean.getData().get(0).getStatus() != null) {
                                    tx_status.setText(bean.getData().get(0).getStatus());
                                } else {
                                    tx_status.setText("");
                                }
                                if (bean.getData().get(0).getRemark() != null) {
                                    tx_remark.setText(bean.getData().get(0).getRemark());
                                } else {
                                    tx_remark.setText("");
                                }
                                /*adapter_my_booking_food = new Adapter_my_booking_food(MyFoodBookingStatus.this, bean.getData());
                                recyclerViewTicketinfo.setLayoutManager(new LinearLayoutManager(MyFoodBookingStatus.this, LinearLayoutManager.VERTICAL, false));
                                recyclerViewTicketinfo.setItemAnimator(new DefaultItemAnimator());
                                recyclerViewTicketinfo.setAdapter(adapter_my_booking_food);*/
                            }else {
                                mSwipeRefreshLayout.setRefreshing(false);
                                progressDialog.dismiss();
                                showtoast(MyBooked_foodStatus_details.this, "No data found");
                            }
                        } else {
                            mSwipeRefreshLayout.setRefreshing(false);
                            progressDialog.dismiss();
                            showtoast(MyBooked_foodStatus_details.this, "No data found");
                        }
                    } else {
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);

                        showtoast(MyBooked_foodStatus_details.this, "Something is wrong");

                    }
                }

                @Override
                public void onFailure(Call<ResponeFoodItemStatus> call, Throwable t) {
                    Log.e("error", t.getMessage());
                    progressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(MyBooked_foodStatus_details.this, "Please Check the Internet Connection", Toast.LENGTH_LONG).show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            }, 2800);
            mSwipeRefreshLayout.setRefreshing(false);

        }
    }
    private void inti(){
        sharedPreference_main = SharedPreference_main.getInstance(this);
        str_food_Id = getIntent().getStringExtra("food_id");
        Log.e("food Id", str_food_Id);
        tx_foodname = findViewById(R.id.txt_food_name_f);
        tx_qty = findViewById(R.id.txt_food_qty);
        tx_amount = findViewById(R.id.txt_paidamount);
        tx_event_name = findViewById(R.id.txt_event_name_f);
        tx_event_venue = findViewById(R.id.txt_event_venue_f);
        tx_event_address = findViewById(R.id.txt_Address);
        tx_time = findViewById(R.id.txt_time_f);
        tx_remark = findViewById(R.id.txt_remark);
        tx_status = findViewById(R.id.txt_status);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_home);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getMyFoodstatusdetails();
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                }
        );
    }
}
