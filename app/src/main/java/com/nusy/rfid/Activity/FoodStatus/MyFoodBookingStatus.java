package com.nusy.rfid.Activity.FoodStatus;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.nusy.rfid.R;
import com.nusy.rfid.adapter.Adapter_my_booking_food;
import com.nusy.rfid.commonMudules.NetworkUtils;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.model.Food.ResponeFoodStatus;
import com.nusy.rfid.retrofit.ApiClient;
import com.nusy.rfid.retrofit.ServiceInterface;
import com.nusy.rfid.utils.DialogsUtils;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusy.rfid.commonMudules.Extension.printLog;
import static com.nusy.rfid.commonMudules.Extension.showtoast;
import static com.nusy.rfid.utils.Constants.Content_Type;

public class MyFoodBookingStatus extends AppCompatActivity {
    SharedPreference_main sharedPreference_main;
    RecyclerView recyclerViewTicketinfo;
    Adapter_my_booking_food adapter_my_booking_food;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_food_booking_status);
        inti();
        getMyFoodMenu();
       /* sharedPreference_main = SharedPreference_main.getInstance(this);
        recyclerViewTicketinfo = findViewById(R.id.recyclerTicketinfo);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_home);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getMyFoodMenu();
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                }
        );*/
    }

    private void getMyFoodMenu() {
        progressDialog = DialogsUtils.showProgressDialog(this, "Please wait...");
        HashMap<String, String> map = new HashMap<>();

        map.put("visitor_id", String.valueOf(sharedPreference_main.getId()));


        if (NetworkUtils.isConnected(MyFoodBookingStatus.this)) {
            progressDialog.show();
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeFoodStatus> call = serviceInterface.MyFoodStatus(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeFoodStatus>() {
                @Override
                public void onResponse(Call<ResponeFoodStatus> call, Response<ResponeFoodStatus> response) {

//                    printLog(response.body().toString());

                    if (response.isSuccessful()) {
                        progressDialog.show();
                        ResponeFoodStatus bean = response.body();
                        printLog(bean.toString());


                        if (bean.getStatus() == 200) {
                            if (bean.getData() != null) {
                                progressDialog.dismiss();
                                mSwipeRefreshLayout.setRefreshing(false);
                                adapter_my_booking_food = new Adapter_my_booking_food(MyFoodBookingStatus.this, bean.getData());
                                recyclerViewTicketinfo.setLayoutManager(new LinearLayoutManager(MyFoodBookingStatus.this, LinearLayoutManager.VERTICAL, false));
                                recyclerViewTicketinfo.setItemAnimator(new DefaultItemAnimator());
                                recyclerViewTicketinfo.setAdapter(adapter_my_booking_food);
                            } else {
                                progressDialog.dismiss();
                                mSwipeRefreshLayout.setRefreshing(false);
                                showtoast(MyFoodBookingStatus.this, "No data found");
                            }
                        } else {
                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);
                            showtoast(MyFoodBookingStatus.this, "No data found");
                        }
                    } else {
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        showtoast(MyFoodBookingStatus.this, "Something is wrong");

                    }
                }

                @Override
                public void onFailure(Call<ResponeFoodStatus> call, Throwable t) {
                    Log.e("error", t.getMessage());
                    progressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(MyFoodBookingStatus.this, "Please Check the Internet Connection", Toast.LENGTH_LONG).show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            }, 2800);
            mSwipeRefreshLayout.setRefreshing(false);

        }
    }

    private void inti() {
        sharedPreference_main = SharedPreference_main.getInstance(this);
        recyclerViewTicketinfo = findViewById(R.id.recyclerTicketinfo);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_home);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getMyFoodMenu();
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                }
        );
    }
}
