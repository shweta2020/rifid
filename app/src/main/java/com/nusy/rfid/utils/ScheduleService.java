package com.nusy.rfid.utils;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.nusy.rfid.R;

public class   ScheduleService extends Fragment {

    private static final String TAG = ScheduleService.class.getSimpleName();

    private TabLayout tabLayout;
    private ViewPager viewPager;


    public ScheduleService() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.schedule_service, container, false);
//
//        tabLayout = (TabLayout)view.findViewById(R.id.tabs);
//        viewPager = (ViewPager)view.findViewById(R.id.view_pager);
//
//        viewPager.setAdapter(new Custom_Fragment_Page_Adapter_Gallery(getChildFragmentManager()));
//        tabLayout.setupWithViewPager(viewPager);

        return view;
    }
}
