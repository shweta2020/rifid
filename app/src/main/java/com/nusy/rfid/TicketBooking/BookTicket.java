package com.nusy.rfid.TicketBooking;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.nusy.rfid.R;
import com.nusy.rfid.adapter.DateAdapter;
import com.nusy.rfid.commonMudules.Extension;
import com.nusy.rfid.commonMudules.NetworkUtils;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.model.DateModel;
import com.nusy.rfid.retrofit.ApiClient;
import com.nusy.rfid.retrofit.ServiceInterface;
import com.nusy.rfid.utils.DialogsUtils;
import com.nusys.rfid.model.ResponseModel;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusy.rfid.commonMudules.Extension.printLog;
import static com.nusy.rfid.commonMudules.Extension.showtoast;
import static com.nusy.rfid.utils.Constants.Content_Type;

public class BookTicket extends AppCompatActivity {
    CalendarView calendarView;
    TextView date;
    Spinner spinner;
    String [] val = {"Cape Towm stadium","sydney"};
    Button buy;
    RecyclerView recyclerView;
    DateAdapter dateAdapter;
    String urlDate = "http://13.233.162.24/rifid/api/web_api/event_date";
    String token="";
    private ArrayList<DateModel> date_model = new ArrayList<>();
    String event_id = "";
    ProgressDialog progressDialog;
    private Dialog dialog;
    SharedPreference_main sharedPreference_main;
    SwipeRefreshLayout mSwipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_book_ticket);

        init();
        datesApi();

    }

    private void init() {
        recyclerView = findViewById(R.id.dateRecycler);
        spinner = findViewById(R.id.spinner);
        spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, val));
        sharedPreference_main=SharedPreference_main.getInstance(this);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_home);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        datesApi();
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                }
        );
        dialog = new Dialog(this);

     /*   dateAdapter = new DateAdapter(BookTicket.this, dateModels);
        recyclerView.setLayoutManager(new LinearLayoutManager(BookTicket.this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(dateAdapter);
       // prepareDate();
*/

     event_id=getIntent().getStringExtra("event_id");
        Log.e("Id Value at Book ticket", "" + sharedPreference_main.getEvent_id());

    }
        private void datesApi() {
            progressDialog = DialogsUtils.showProgressDialog(this, "Please wait...");
            HashMap<String, String> map = new HashMap<>();
            map.put("event_id", sharedPreference_main.getEvent_id());
            if (NetworkUtils.isConnected(BookTicket.this)) {
                progressDialog.show();
                ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
                Call<ResponseModel> call = serviceInterface.getEventDate(sharedPreference_main.getToken(), Content_Type, map);
                call.enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
   //                    printLog(response.body().toString());
                        if (response.isSuccessful()) {

                            ResponseModel bean=response.body();
                            printLog(bean.toString());
                            if (bean.getStatus()==200) {
                                if (bean.getData() != null) {
                                    progressDialog.dismiss();
                                    mSwipeRefreshLayout.setRefreshing(false);
                                    dateAdapter = new DateAdapter(BookTicket.this, bean.getData());
                                    recyclerView.setLayoutManager(new LinearLayoutManager(BookTicket.this, LinearLayoutManager.VERTICAL, false));
                                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                                    recyclerView.setAdapter(dateAdapter);
                                }else {
                                    progressDialog.dismiss();
                                    showtoast(BookTicket.this,"No data found");
                                }
                            }
                            else {
                                progressDialog.dismiss();
                                showtoast(BookTicket.this,"No data found");
                            }
                        } else {
                            showtoast(BookTicket.this,"Something is wrong");
                            mSwipeRefreshLayout.setRefreshing(false);
                            progressDialog.dismiss();
                        }
                    }
                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                      //  printLog(t.getMessage());
                        progressDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
            }else {
                Extension.showErrorDialog(BookTicket.this,dialog);
                mSwipeRefreshLayout.setRefreshing(false);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                    }
                }, 2800);
                mSwipeRefreshLayout.setRefreshing(false);

            }
        }

    }

