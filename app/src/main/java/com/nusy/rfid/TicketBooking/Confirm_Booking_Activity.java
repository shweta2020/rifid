package com.nusy.rfid.TicketBooking;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.internal.$Gson$Preconditions;
import com.nusy.rfid.MainActivity;
import com.nusy.rfid.R;
import com.nusy.rfid.commonMudules.NetworkUtils;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.model.Parking.ResponeParking;
import com.nusy.rfid.retrofit.ApiClient;
import com.nusy.rfid.retrofit.ServiceInterface;
import com.nusy.rfid.utils.DialogsUtils;
import com.nusy.rfid.utils.PaypalConfig;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusy.rfid.commonMudules.Extension.printLog;
import static com.nusy.rfid.commonMudules.Extension.showtoast;
import static com.nusy.rfid.utils.Constants.Content_Type;

public class Confirm_Booking_Activity extends AppCompatActivity {
    TextView tx_event_name;
    TextView tx_event_venue;
    TextView tx_event_date;
    TextView tx_event_time;
    TextView tx_name;
    TextView tx_mobile, tx_total;
    TextView tx_email, tx_final_amount, tx_parking_amount;
    ImageView im_event;
    Button bt_pay;
    String name, email, mobile, address, pin, id, vehicle_amount, paymentDetails, str_transationId;
    int total;
    ImageView imb_paypal, imb_ozow;
    Dialog dialog;
    ProgressDialog progressDialog;
    SharedPreference_main sharedPreference_main;
    //Paypal  request code
    public static final int PAYPAL_REQUEST_CODE = 123;

    //Paypal Configuration Object
    private static PayPalConfiguration config = new PayPalConfiguration() // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(PaypalConfig.PAYPAL_CLIENT_ID)
            .merchantName("TechNxt Code Labs")
            .merchantPrivacyPolicyUri(
                    Uri.parse("https://www.paypal.com/webapps/mpp/ua/privacy-full"))
            .merchantUserAgreementUri(
                    Uri.parse("https://www.paypal.com/webapps/mpp/ua/useragreement-full"));  // or live (ENVIRONMENT_PRODUCTION)


    Button subscribe;
    private String paymentAmount = "1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm__booking_);
        sharedPreference_main = SharedPreference_main.getInstance(this);
        tx_email = findViewById(R.id.txt_email);
        tx_event_name = findViewById(R.id.txt_event_name);
        tx_mobile = findViewById(R.id.txt_mobile);
        tx_final_amount = findViewById(R.id.txt_final_amount);
        tx_event_venue = findViewById(R.id.txt_event_venue);
        tx_event_date = findViewById(R.id.txt_event_date);
        tx_event_time = findViewById(R.id.txt_event_time);
        tx_parking_amount = findViewById(R.id.txt_parking_amount);
        tx_name = findViewById(R.id.txt_name);
        im_event = findViewById(R.id.img_event);
        tx_total = findViewById(R.id.txt_total);
        name = getIntent().getStringExtra("name");
        email = getIntent().getStringExtra("email");
        pin = getIntent().getStringExtra("pin");
        mobile = getIntent().getStringExtra("mobile");
        address = getIntent().getStringExtra("address");
        vehicle_amount = getIntent().getStringExtra("amount");
        id = getIntent().getStringExtra("id");

        tx_name.setText(name);
        tx_mobile.setText(mobile);
        tx_email.setText(email);
        String str_event_name = sharedPreference_main.getEvent_name();
        tx_event_name.setText(str_event_name);
        String str_event_venue = sharedPreference_main.getEvent_venue();
        tx_event_venue.setText(str_event_venue);
        String str_event_time = sharedPreference_main.getEvent_time();
        tx_event_time.setText(str_event_time);
        // tx_event_time.setText(str_event_time);
        String str_date = sharedPreference_main.getEvent_date();
        tx_event_date.setText(str_date);
        String str_amount = sharedPreference_main.getEvent_amount();
        tx_final_amount.setText(str_amount);
        String str_img = sharedPreference_main.getEvent_image();
        if (vehicle_amount != null) {
            tx_parking_amount.setText(vehicle_amount);
            total = Integer.parseInt(str_amount) + Integer.parseInt(vehicle_amount);
        } else {
            total = Integer.parseInt(str_amount) + 0;
            tx_parking_amount.setText("0");

        }
        tx_total.setText(String.valueOf(total));

        Picasso.with(getApplicationContext()).load(str_img).fit().centerCrop()
                .placeholder(R.drawable.event)
                .error(R.drawable.event)
                .into(im_event);


        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
        dialog = new Dialog(Confirm_Booking_Activity.this);
        // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.payment_dialog);
        //dialog.show();
        bt_pay = findViewById(R.id.btn_amount);
        bt_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ticket_Booked();
                ///dialog = new Dialog(Confirm_Booking_Activity.this);
                // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                //dialog.setContentView(R.layout.payment_dialog);
                dialog.show();
                //getPayment();
            }
        });
        imb_ozow = dialog.findViewById(R.id.ozow);
        imb_paypal = dialog.findViewById(R.id.paypal);
        imb_paypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPayment();
            }
        });
        imb_ozow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Coming Soon", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getPayment() {

        //Creating a paypalpayment
        PayPalPayment payment = new PayPalPayment(new
                BigDecimal(total), "USD", "Test", PayPalPayment.PAYMENT_INTENT_SALE);

        //Creating Paypal Payment activity intent
        Intent intent = new Intent(Confirm_Booking_Activity.this, PaymentActivity.class);
        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        startActivityForResult(intent, PAYPAL_REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAYPAL_REQUEST_CODE) {
            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                Log.d("CONFIRM", String.valueOf(confirm));
                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        paymentDetails = confirm.toJSONObject().toString(4);
                        Log.d("paymentExample", paymentDetails);
                        Log.i("paymentExample", paymentDetails);
                        Log.e("Payment ID", paymentDetails);
                        Log.d("Pay Confirm : ", String.valueOf(confirm.getPayment().toJSONObject()));
//                        Starting a new activity for the payment details and status to show
                        try {
                            JSONObject jsonDetails = new JSONObject(paymentDetails);

                            //Displaying payment details
                            showDetails(jsonDetails.getJSONObject("response"));
                            //str_transationId = jsonDetails.getString("id");
                        } catch (JSONException e) {
                            Toast.makeText(Confirm_Booking_Activity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                        ticket_Booked();

                      /*  startActivity(new Intent(Confirm_Booking_Activity.this, PaymentDetails.class)
                                .putExtra("PaymentDetails", paymentDetails));*/

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred : ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    private void showDetails(JSONObject jsonDetails) throws JSONException {
        //Views
        // TextView textViewId = (TextView) findViewById(R.id.paymentId);
        // TextView textViewStatus = (TextView) findViewById(R.id.paymentStatus);

        //Showing the paypal payment details and status from json object

        str_transationId = jsonDetails.getString("id");
        //  textViewId.setText(jsonDetails.getString("id"));
        //  textViewStatus.setText(jsonDetails.getString("state"));

    }

    private void ticket_Booked() {
        progressDialog = DialogsUtils.showProgressDialog(this, "Please wait...");
        HashMap<String, String> map = new HashMap<>();
        map.put("event_id", sharedPreference_main.getEvent_id());
        map.put("user_id", String.valueOf(sharedPreference_main.getId()));
        map.put("name", name);
        map.put("email", email);
        map.put("phone_no", mobile);
        map.put("address", address);
        map.put("pincode", pin);
        map.put("paymeny_type", String.valueOf(2));
        map.put("transaction_id", str_transationId);
        map.put("total_amount", String.valueOf(total));
        map.put("event_date", sharedPreference_main.getEvent_date());
        map.put("parking_status", String.valueOf(1));
        map.put("parking_type_id", String.valueOf(id));
        map.put("parking_qty", String.valueOf(1));
        map.put("ticket_id", sharedPreference_main.getticket());
        map.put("qty", sharedPreference_main.getQty());
        map.put("amount", sharedPreference_main.getamt());

        if (NetworkUtils.isConnected(Confirm_Booking_Activity.this)) {
            progressDialog.show();
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeParking> call = serviceInterface.getticketBook(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeParking>() {
                @Override
                public void onResponse(Call<ResponeParking> call, Response<ResponeParking> response) {

//                    printLog(response.body().toString());

                    if (response.isSuccessful()) {
                        progressDialog.show();
                        ResponeParking bean = response.body();
                        printLog(bean.toString());


                        if (bean.getStatus() == 200) {
                            progressDialog.dismiss();
                            /* if (bean.getData() != null) {*/
                            showtoast(Confirm_Booking_Activity.this, "Booking Successfully");
                            Intent intent = new Intent(Confirm_Booking_Activity.this, MainActivity.class);
                            startActivity(intent);
                            // }
                        } else {
                            progressDialog.dismiss();
                            showtoast(Confirm_Booking_Activity.this, "No data found");
                        }
                    } else {
                        progressDialog.dismiss();
                        showtoast(Confirm_Booking_Activity.this, "Something is wrong");

                    }
                }

                @Override
                public void onFailure(Call<ResponeParking> call, Throwable t) {
                    Log.e("error", t.getMessage());
                    progressDialog.dismiss();
                }
            });
        } else {
            Toast.makeText(Confirm_Booking_Activity.this, "Please Check the Internet Connection", Toast.LENGTH_LONG).show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            }, 2800);
            //  mSwipeRefreshLayout.setRefreshing(false);


        }
    }
}
