package com.nusy.rfid.TicketBooking;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.nusy.rfid.R;
import com.nusy.rfid.adapter.TicketTypeAdapter;
import com.nusy.rfid.commonMudules.Extension;
import com.nusy.rfid.commonMudules.NetworkUtils;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.model.DateModel;
import com.nusy.rfid.model.TickeTypeModel;
import com.nusy.rfid.retrofit.ApiClient;
import com.nusy.rfid.retrofit.ServiceInterface;
import com.nusy.rfid.utils.DialogsUtils;
import com.nusys.rfid.model.ResponseModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusy.rfid.commonMudules.Extension.printLog;
import static com.nusy.rfid.commonMudules.Extension.showtoast;
import static com.nusy.rfid.utils.Constants.Content_Type;

public class ActivityTicketType extends AppCompatActivity {
    String event_date = "";
    String event_id = "";
    private List<DateModel> modelList;
    String urlTicket = "http://13.233.162.24/rifid/api/web_api/event_ticket_list";
    private ArrayList<TickeTypeModel> ticket_model = new ArrayList<>();
    TicketTypeAdapter ticketAdapter;
    RecyclerView recyclerViewTicket;
    String event, date, time, venue;
    TextView evnt, evdt, evtm, evvnue;
    Button proceed;
    private Dialog dialog;
    ProgressDialog progressDialog;
    SwipeRefreshLayout mSwipeRefreshLayout;
    SharedPreference_main sharedPreference_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_ticket_type);
        sharedPreference_main = SharedPreference_main.getInstance(this);

        init();

        getTicketType();

        sharedPreference_main = SharedPreference_main.getInstance(this);

        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                      getTicketType();
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                }
        );
       /* if (mSwipeRefreshLayout!=null){
            mSwipeRefreshLayout.setRefreshing(false);
        }*/
   /*     mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getTicketType();
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                }
        );*/

    }

    private void getTicketType() {
        progressDialog = DialogsUtils.showProgressDialog(this, "Please wait...");
        HashMap<String, String> map = new HashMap<>();
        map.put("event_id", sharedPreference_main.getEvent_id());
        map.put("date", event_date);
        if (NetworkUtils.isConnected(ActivityTicketType.this)) {
            progressDialog.show();
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponseModel> call = serviceInterface.getEvent_ticket_list(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponseModel>() {
                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

//                    printLog(response.body().toString());

                    if (response.isSuccessful()) {
                        progressDialog.show();
                        mSwipeRefreshLayout.setRefreshing(false);
                        ResponseModel bean = response.body();
                        printLog(bean.toString());

                        if (bean.getStatus() == 200) {

                            if (bean.getData() != null) {
                                progressDialog.dismiss();

                                ticketAdapter = new TicketTypeAdapter(ActivityTicketType.this, bean.getData());
                                recyclerViewTicket.setLayoutManager(new LinearLayoutManager(ActivityTicketType.this, LinearLayoutManager.VERTICAL, false));
                                recyclerViewTicket.setItemAnimator(new DefaultItemAnimator());
                                recyclerViewTicket.setAdapter(ticketAdapter);
                            }else {
                                progressDialog.dismiss();
                                showtoast(ActivityTicketType.this, "No data found");
                            }
                        } else {
                            progressDialog.dismiss();
                            showtoast(ActivityTicketType.this, "No data found");
                        }
                    } else {
                        showtoast(ActivityTicketType.this, "Something is wrong");
                        mSwipeRefreshLayout.setRefreshing(false);
                        progressDialog.dismiss();
                    }
                }
                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
          /*        try {
                    printLog(t.getMessage());
                  }catch (Exception ex){
                      String err = (t.getMessage()==null)?"Server failed":t.getMessage();
                      Log.e("Server failed-err2:",err);
                  }*/
                    progressDialog.dismiss();
                }
            });
        } else {
            Extension.showErrorDialog(ActivityTicketType.this, dialog);
            //mSwipeRefreshLayout.setRefreshing(false);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            }, 2800);
            mSwipeRefreshLayout.setRefreshing(false);

        }
    }

    private void init() {
        evnt = findViewById(R.id.txt_event);
        evdt = findViewById(R.id.txt_date);
        evtm = findViewById(R.id.txt_time);
        evvnue = findViewById(R.id.txt_venue_place);
        proceed = findViewById(R.id.proceed);
//        ticketsApi();
        recyclerViewTicket = findViewById(R.id.recyclerTicketType);
        dialog = new Dialog(this);
//
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_home);
        event_date = getIntent().getStringExtra("date");
        event_id = getIntent().getStringExtra("id");
        event = getIntent().getStringExtra("event_name");
        time = getIntent().getStringExtra("event_time");
        venue = getIntent().getStringExtra("venue");
        Log.e("date on ticket details", "" + event_date);
        Log.e("id on ticket details", "nhi ayya" + event_id);
        evnt.setText(event);
        evdt.setText(event_date);
        evtm.setText(time);
        evvnue.setText(venue);
        Log.e("tag", "" + event);
    }
}
