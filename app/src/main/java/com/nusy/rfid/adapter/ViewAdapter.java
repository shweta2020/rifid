package com.nusy.rfid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusy.rfid.R;
import com.nusy.rfid.model.ViewEventModel;

import java.util.List;

public class ViewAdapter extends RecyclerView.Adapter<ViewAdapter.MyViewHolder> {
    Context ctx;
    private List<ViewEventModel> modelList;

    public ViewAdapter(Context ctx, List<ViewEventModel> modelLists) {
        this.ctx = ctx;
        this.modelList = modelLists;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.img_adapter, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {
        ViewEventModel model = modelList.get(position);
        Glide.with(ctx).load(model.getImg()).into(myViewHolder.title);




    }

    @Override
    public int getItemCount() {

        return modelList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView title;
        public TextView year, genre,date;

        public MyViewHolder(View view) {
            super(view);
            title = (ImageView) view.findViewById(R.id.title);
            //date = (TextView) view.findViewById(R.id.date);
        }
    }
}