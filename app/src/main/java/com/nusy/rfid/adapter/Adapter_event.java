package com.nusy.rfid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusy.rfid.R;
import com.nusy.rfid.model.Ticket_booking_info.ResponeTicketBook;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Adapter_event extends RecyclerView.Adapter<Adapter_event.myholder> {
    Context ctx;
    private List<ResponeTicketBook.Datat.EventDetail> modelList;

    public Adapter_event(Context ctx, List<ResponeTicketBook.Datat.EventDetail> modelList) {
        this.ctx = ctx;
        this.modelList = modelList;
    }

    @Override
    public myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_event, parent, false);

        return new myholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull myholder holder, int position) {
        holder.txt_event_name.setText(modelList.get(position).getEventName());
        Picasso.with(ctx).load(modelList.get(position).getImage()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView txt_event_name;

        public myholder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.event_image);
            txt_event_name = itemView.findViewById(R.id.event_name_list);
        }
    }
}
