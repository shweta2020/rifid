package com.nusy.rfid.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusy.rfid.Activity.FoodBooking.Food_items;
import com.nusy.rfid.R;
import com.nusy.rfid.model.Food.ResponeFoodMenu;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Adapter_food_list extends RecyclerView.Adapter<Adapter_food_list.myholder> {
    @NonNull


    Context ctx;
    private List<ResponeFoodMenu.Dataf> modelList;

    public Adapter_food_list(@NonNull Context ctx, List<ResponeFoodMenu.Dataf> modelList) {
        this.ctx = ctx;
        this.modelList = modelList;
    }

    public Adapter_food_list.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_of_food_menus, parent, false);

        return new Adapter_food_list.myholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter_food_list.myholder holder, final int position) {
        holder.tx_price.setText(modelList.get(position).getAmount());
        holder.tx_food_name.setText(modelList.get(position).getFoodItem());
        Picasso.with(ctx).load(modelList.get(position).getImage()).into(holder.im_food);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, Food_items.class);
                intent.putExtra("food_id", modelList.get(position).getFoodId());
                ctx.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class myholder extends RecyclerView.ViewHolder {

        TextView tx_food_name, tx_price;
        ImageView im_food;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tx_food_name = itemView.findViewById(R.id.txt_food_name);
            tx_price = itemView.findViewById(R.id.txt_food_price);
            im_food = itemView.findViewById(R.id.img_food_item);
        }
    }
}
