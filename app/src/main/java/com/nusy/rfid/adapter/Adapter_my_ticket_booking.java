package com.nusy.rfid.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusy.rfid.Activity.FoodBooking.Food_item_list;
import com.nusy.rfid.R;
import com.nusy.rfid.Activity.TicketBooking.Show_QR_codeActivity;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.model.Response_my_booking.ResponeBook;

import java.util.List;

public class Adapter_my_ticket_booking extends RecyclerView.Adapter<Adapter_my_ticket_booking.MyViewHolder> {
    @NonNull
    String str_eventId;
    Context ctx;
    private List<ResponeBook.Datum> modelList;
    SharedPreference_main sharedPreference_main;

    public Adapter_my_ticket_booking(@NonNull Context ctx, List<ResponeBook.Datum> modelList) {
        this.ctx = ctx;
        this.modelList = modelList;
    }

    public Adapter_my_ticket_booking.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ticket_info_list, parent, false);
        sharedPreference_main = SharedPreference_main.getInstance(ctx);

        return new Adapter_my_ticket_booking.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter_my_ticket_booking.MyViewHolder holder, final int position) {
        //holder.tx_parking.setText(modelList.get(position).getEventName());
        if ((modelList.get(position).getTicket().getParking()) != null) {
            holder.tx_parking.setText(modelList.get(position).getTicket().getParking().getParkingTotal().toString());
        } else {
            holder.tx_parking.setText("No Parking has been booked");
            holder.tx_parking.setText("");
            //  holder.tx_parking.setText(modelList.get(position).getTicket().getParking().toString());
        }
        if ((modelList.get(position).getTicket().getName() != null)) {
            holder.tx_name.setText(modelList.get(position).getTicket().getName().toString());
        } else {
            holder.tx_name.setText(" ");
        }
        holder.tx_event_name.setText(modelList.get(position).getEventName().toString());
        holder.tx_venue.setText(modelList.get(position).getEventVenue().toString());
        holder.tx_time.setText(modelList.get(position).getEventTime().toString());
        holder.tx_qty.setText(modelList.get(position).getTicket().getQty().toString());
        holder.tx_total_amount.setText(modelList.get(position).getTicket().getTotal().toString());
        holder.tx_amount.setText(modelList.get(position).getTicket().getSubTotal().toString());
        holder.tx_qr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, Show_QR_codeActivity.class);
                intent.putExtra("event_id", modelList.get(position).getEventId());
                intent.putExtra("order_id",modelList.get(position).getOrderId());
                intent.putExtra("event_date",modelList.get(position).getEventDate());
                intent.putExtra("ticket",modelList.get(position).getTicket().getQty());
                ctx.startActivity(intent);
            }
        });
        //holder.tx_parking.setText( modelList.get(position).getTicket().getParking().toString());
        str_eventId = modelList.get(position).getEventId();
        //sharedPreference_main.setfoodorderid(modelList.get(position).getOrderId());
        Log.e("event Id", str_eventId);
        holder.bt_food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, Food_item_list.class);
                intent.putExtra("event_id", str_eventId);
                ctx.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tx_name, tx_venue, tx_time, tx_amount, tx_parking, tx_qty, tx_event_name, tx_total_amount, tx_qr;
        Button bt_food;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tx_qty = itemView.findViewById(R.id.txt_qty);
            tx_amount = itemView.findViewById(R.id.txt_amount);
            tx_name = itemView.findViewById(R.id.txt_name_i);
            tx_parking = itemView.findViewById(R.id.txt_Parking_amount);
            tx_time = itemView.findViewById(R.id.txt_time);
            tx_venue = itemView.findViewById(R.id.txt_venue);
            tx_event_name = itemView.findViewById(R.id.txt_event_name);
            tx_total_amount = itemView.findViewById(R.id.txt_total_amount);
            bt_food = itemView.findViewById(R.id.btn_food);
            tx_qr = itemView.findViewById(R.id.txt_qr);

        }
    }

    /*private void food_booking() {
        HashMap<String, String> map = new HashMap<>();
        map.put("event_id", str_eventId);

        if (NetworkUtils.isConnected(ctx)) {

            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeFoodMenu> call = serviceInterface.FoodMenu(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeFoodMenu>() {
                @Override
                public void onResponse(Call<ResponeFoodMenu> call, Response<ResponeFoodMenu> response) {

//                    printLog(response.body().toString());

                    if (response.isSuccessful()) {

                        ResponeFoodMenu bean = response.body();
                        // printLog(bean.toString());
                        if (bean.getStatus() == 200) {
                            if (bean.getData() != null) {

                              *//*  ticketAdapterinfo = new Adapter_my_ticket_booking(MyTicketBookingInfoActivity.this, bean.getData());
                                recyclerViewTicketinfo.setLayoutManager(new LinearLayoutManager(MyTicketBookingInfoActivity.this, LinearLayoutManager.VERTICAL, false));
                                recyclerViewTicketinfo.setItemAnimator(new DefaultItemAnimator());
                                recyclerViewTicketinfo.setAdapter(ticketAdapterinfo);*//*
                            }
                        } else {
                            showtoast(ctx, "No data found");
                        }
                    } else {
                        showtoast(ctx, "Something is wrong");
                    }
                }

                @Override
                public void onFailure(Call<ResponeFoodMenu> call, Throwable t) {

                }


            });
        } else {

        }
    }*/
}
