package com.nusy.rfid.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nusy.rfid.R;
import com.nusy.rfid.model.CartModel;
import com.nusy.rfid.utils.MyApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {
    Context ctx;
    private List<CartModel> modelList;
    String deleteCartUrl = "http://13.233.162.24/rifid/api/web_api/delete_item";



    public CartAdapter(Context ctx, List<CartModel> modelLists) {
        this.ctx = ctx;
        this.modelList = modelLists;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView eventName,eventVenue,eventTime,eventDate,eventTicket,total;
        public Button delete;


        public MyViewHolder(View view) {
            super(view);

            eventName = (TextView) view.findViewById(R.id.event_name);
            eventVenue = (TextView) view.findViewById(R.id.event_venue);
            eventTime = (TextView) view.findViewById(R.id.event_time);
            eventDate = (TextView) view.findViewById(R.id.event_date);
            eventTicket = (TextView) view.findViewById(R.id.event_ticket);
            total = (TextView) view.findViewById(R.id.event_totall);
            delete = (Button)view.findViewById(R.id.event_delete);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cart_adapter, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, final int position) {
       final CartModel model = modelList.get(position);
        Log.e("fghjkl","lkjhgfdsa"+model.getEventName());
        myViewHolder.eventName.setText(model.getEventName());
        myViewHolder.eventVenue.setText(model.getEventVenue());
        myViewHolder.eventTime.setText(model.getEventTime());
        myViewHolder.eventDate.setText(model.getEventDate());
        myViewHolder.eventTicket.setText(model.getTicket());
        myViewHolder.total.setText(model.getTotal());
        myViewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteCartApi(model.getId());
                removeAt(getItemViewType(position));

            }
        });
    }


    @Override
    public int getItemCount() {

        return modelList.size();
    }

    private void deleteCartApi(final String id) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, deleteCartUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            Log.e("TAG", "LEAD_jsonObject" + response);
                            JSONObject object = new JSONObject(response);
                            Toast.makeText(ctx, ""+object.getString("msg"), Toast.LENGTH_SHORT).show();


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("JSON Parser", "Error parsing data [" + e.getMessage() + "] ");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("TAG", "onErrorResponse: " + String.valueOf(error));

            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("id", id);


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                return headers;
            }
        };

        MyApplication.getInstance().addToRequestQueue(stringRequest);

    }
    public void removeAt(int position) {
        modelList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, modelList.size());
    }


}