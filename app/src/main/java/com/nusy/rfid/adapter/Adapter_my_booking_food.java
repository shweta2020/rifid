package com.nusy.rfid.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusy.rfid.Activity.FoodStatus.MyBooked_foodStatus_details;
import com.nusy.rfid.R;
import com.nusy.rfid.model.Food.ResponeFoodStatus;

import java.util.List;

public class Adapter_my_booking_food extends RecyclerView.Adapter<Adapter_my_booking_food.myholder> {
    @NonNull
    Context ctx;
    private List<ResponeFoodStatus.Datafood> modelList;

    public Adapter_my_booking_food(@NonNull Context ctx, List<ResponeFoodStatus.Datafood> modelList) {
        this.ctx = ctx;
        this.modelList = modelList;
    }

    public myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mybookedfoodlist, parent, false);

        return new Adapter_my_booking_food.myholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull myholder holder, final int position) {
        if ((modelList.get(position).getStatus() != null)) {
            holder.tx_status.setText(modelList.get(position).getStatus());
        } else {
            holder.tx_status.setText("");
        }
        if (modelList.get(position).getRemark() != null) {
            holder.tx_remak.setText(modelList.get(position).getRemark());
        } else {
            holder.tx_remak.setText("");

        }

        holder.tx_time.setText(modelList.get(position).getTime());
        holder.tx_amount.setText(modelList.get(position).getTotalAmount());
        holder.tx_qty.setText(modelList.get(position).getQuantity());
        holder.tx_item_name.setText(modelList.get(position).getFoodName());
        holder.tx_event_name.setText(modelList.get(position).getEventName());
        holder.tx_event_venue.setText(modelList.get(position).getEventVenue());
        holder.tx_event_address.setText(modelList.get(position).getEventAddress());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, MyBooked_foodStatus_details.class);
                intent.putExtra("food_id", modelList.get(position).getId());
                ctx.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tx_item_name, tx_qty, tx_amount, tx_event_name, tx_event_venue, tx_event_address, tx_time, tx_remak, tx_status;

        public myholder(@NonNull View itemView) {
            super(itemView);

            tx_amount = itemView.findViewById(R.id.txt_paidamount);
            tx_event_address = itemView.findViewById(R.id.txt_Address);
            tx_event_name = itemView.findViewById(R.id.txt_event_name_f);
            tx_event_venue = itemView.findViewById(R.id.txt_event_venue_f);
            tx_item_name = itemView.findViewById(R.id.txt_food_name_f);
            tx_qty = itemView.findViewById(R.id.txt_food_qty);
            tx_time = itemView.findViewById(R.id.txt_time_f);
            tx_remak = itemView.findViewById(R.id.txt_remark);
            tx_status = itemView.findViewById(R.id.txt_status);

        }
    }
}
