package com.nusy.rfid.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusy.rfid.R;
import com.nusy.rfid.Activity.TicketBooking.ActivityTicketType;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusys.rfid.model.Data;

import java.util.List;

public class DateAdapter extends RecyclerView.Adapter<DateAdapter.MyViewHolder> {
    Context ctx;
    private List<Data> modelList;
    SharedPreference_main sharedPreference_main;
    public DateAdapter(Context ctx, List<Data> modelLists) {
        this.ctx = ctx;
        this.modelList = modelLists;
        sharedPreference_main= SharedPreference_main.getInstance(ctx);
    }

    @NonNull
    @Override
    public DateAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.date_adapter, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final DateAdapter.MyViewHolder myViewHolder, final int position) {
        myViewHolder.date.setText(modelList.get(position).getDate());
        myViewHolder.evname.setText(modelList.get(position).getEvent_name());
        myViewHolder.evtime.setText(modelList.get(position).getEvent_time());
        myViewHolder.evvenue.setText(modelList.get(position).getEvent_venue());


        sharedPreference_main.setEvent_date(modelList.get(position).getDate());
        myViewHolder.id.setText(modelList.get(position).getEvent_id());
        Log.e("rfbgjkfgjkh", "" + modelList.get(position).getEvent_id());
        myViewHolder.btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String dateOfBtn = myViewHolder.date.getText().toString();
                String id = myViewHolder.id.getText().toString();
                Log.e("Tag", "gdsghdhfhdfhdgh" + id);

                Intent intent = new Intent(ctx, ActivityTicketType.class);

                intent.putExtra("date", dateOfBtn);
                intent.putExtra("id", modelList.get(position).getEvent_id());
                intent.putExtra("event_name", modelList.get(position).getEvent_name());
                intent.putExtra("event_time", modelList.get(position).getEvent_time());
                intent.putExtra("venue", modelList.get(position).getEvent_venue());
                ctx.startActivity(intent);


//          Fragment fragment = new TicketType();
//                Bundle bundle = new Bundle();
//                bundle.putString("date",dateOfBtn);
//                bundle.putString("id",modelList.get(position).getEvent_id());
//                bundle.putString("event_name",modelList.get(position).getEvent_name());
//                bundle.putString("event_time",modelList.get(position).getEvent_time());
//                bundle.putString("venue",modelList.get(position).getEvent_venue());
//                fragment.setArguments(bundle);
//                FragmentManager fragmentManager = ((AppCompatActivity)ctx).getSupportFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.contentmain, fragment);
//                fragmentTransaction.addToBackStack(null);
//                fragmentTransaction.commit();
            }
        });
        //   myViewHolder.genre.setText(model.getTittle());
//        myViewHolder.year.setText(model.getLocation());
//        myViewHolder.date.setText(model.getDate());


    }

    @Override
    public int getItemCount() {

        return modelList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView date, id, evname, evdate, evtime, evvenue;
        public Button btnGo;

        public MyViewHolder(View view) {
            super(view);

            date = (TextView) view.findViewById(R.id.date);
            id = (TextView) view.findViewById(R.id.id);
            evname = (TextView) view.findViewById(R.id.evnt);
            evtime = (TextView) view.findViewById(R.id.evnttime);
            evvenue = (TextView) view.findViewById(R.id.evntvnu);

            btnGo = (Button) view.findViewById(R.id.go);
        }
    }
}