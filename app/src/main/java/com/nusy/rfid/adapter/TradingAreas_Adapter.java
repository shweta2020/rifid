package com.nusy.rfid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusy.rfid.R;
import com.nusy.rfid.model.DataTradingAreasModel;

import java.util.List;

public class TradingAreas_Adapter extends RecyclerView.Adapter<TradingAreas_Adapter.MyViewHolder> {
    Context ctx;
    private List<DataTradingAreasModel> modelList;

    public TradingAreas_Adapter(Context ctx, List<DataTradingAreasModel> modelLists) {
        this.ctx = ctx;
        this.modelList = modelLists;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.trading_adapter, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        DataTradingAreasModel model = modelList.get(position);
        Glide.with(ctx).load(model.getImg()).into(myViewHolder.img);
        myViewHolder.tittle.setText(model.getTitles());
//        myViewHolder.year.setText(model.getLocation());
//        myViewHolder.date.setText(model.getDate());
    }

    @Override
    public int getItemCount() {

        return modelList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView img;
        public TextView tittle;

        public MyViewHolder(View view) {
            super(view);
            img = (ImageView) view.findViewById(R.id.img);
            tittle = (TextView) view.findViewById(R.id.tittle);

        }
    }
}