package com.nusy.rfid.adapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusy.rfid.R;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.fragment.EventDetails;
import com.nusys.rfid.model.Data;

import java.util.List;


public class EventAdapter extends RecyclerView.Adapter<EventAdapter.MyViewHolder> {
    Context ctx;
    private List<Data> modelList;
    SharedPreference_main sharedPreference_main;

    public EventAdapter(Context ctx, List<Data> modelLists) {
        this.ctx = ctx;
        this.modelList = modelLists;
        sharedPreference_main = SharedPreference_main.getInstance(ctx);

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.event_adapter, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, final int position) {
        Glide.with(ctx).load(modelList.get(position).getImage()).into(myViewHolder.title);
//        Log.e("Tag","sec=="+model.getEvent_name());
        myViewHolder.event_id.setText(modelList.get(position).getId());
        myViewHolder.event_name.setText(modelList.get(position).getEvent_name());
        myViewHolder.event_time.setText("at: " + modelList.get(position).getEvent_time());
        myViewHolder.datestart.setText("Start on :  " + modelList.get(position).getStart_date());
        myViewHolder.dateEnd.setText("End on: " + modelList.get(position).getEnd_date());
        myViewHolder.address.setText(modelList.get(position).getEvent_address());
        myViewHolder.maxVisitor.setText(modelList.get(position).getMax_visitor());
        myViewHolder.eventtype.setText(modelList.get(position).getEvent_type());
        myViewHolder.event_venue.setText(modelList.get(position).getEvent_venue());
        String val = modelList.get(position).getEvent_type();
        Log.e("qwety", "aaaaaaayya" + val);
        if (val.equals("0")) {
            myViewHolder.amount.setText("Free");
        } else {
            myViewHolder.amount.setText(modelList.get(position).getAmount());
        }

        myViewHolder.linear_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("qwqwqwqw", "vvvvvvvvvvvvvvvvvvvvvvvvvvv" + myViewHolder.event_id.getText());


                Fragment fragment = new EventDetails();

                Bundle bundle = new Bundle();
                sharedPreference_main.setEvent_id(modelList.get(position).getId());

                sharedPreference_main.setEvent_name(modelList.get(position).getEvent_name());
                sharedPreference_main.setEvent_venue(modelList.get(position).getEvent_address());

                sharedPreference_main.setEvent_time(modelList.get(position).getEvent_time());
                sharedPreference_main.setEvent_image(modelList.get(position).getImage());

                bundle.putString("id", myViewHolder.event_id.getText().toString());
                bundle.putString("name", myViewHolder.event_name.getText().toString());
                bundle.putString("address", myViewHolder.address.getText().toString());
                bundle.putString("startDate", myViewHolder.datestart.getText().toString());
                bundle.putString("endDate", myViewHolder.dateEnd.getText().toString());
                bundle.putString("time", myViewHolder.event_time.getText().toString());
                bundle.putString("amount", myViewHolder.amount.getText().toString());
                bundle.putString("maxVisitor", myViewHolder.maxVisitor.getText().toString());
                bundle.putString("eventType", myViewHolder.eventtype.getText().toString());
                //bundle.putString("eventimage", myViewHolder.title.toString());

                bundle.putString("image", modelList.get(position).getImage());

                bundle.putString("eventvenue", myViewHolder.event_venue.getText().toString());
                Log.e("image Url", modelList.get(position).getImage());
                Log.e("event name", myViewHolder.event_venue.toString());
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = ((AppCompatActivity) ctx).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.contentmain, fragment);
                fragmentTransaction.addToBackStack("detail");
                fragmentTransaction.commit();

            }
        });

    }

    @Override
    public int getItemCount() {

        return modelList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView title;
        LinearLayout linear_buy;
        public TextView event_name, event_time, datestart, dateEnd, amount, address, maxVisitor, eventtype, event_id, event_venue;

        public MyViewHolder(View view) {
            super(view);
            title = (ImageView) view.findViewById(R.id.title);
            event_name = (TextView) view.findViewById(R.id.eventName);
            event_time = (TextView) view.findViewById(R.id.time);
            datestart = (TextView) view.findViewById(R.id.startdate);
            dateEnd = (TextView) view.findViewById(R.id.enddate);
            address = (TextView) view.findViewById(R.id.address);
            maxVisitor = (TextView) view.findViewById(R.id.maxVisitor);
            eventtype = (TextView) view.findViewById(R.id.eventtype);
            amount = (TextView) view.findViewById(R.id.amount);
            event_id = (TextView) view.findViewById(R.id.eventId);
            event_venue = view.findViewById(R.id.event_venu);
            linear_buy = (LinearLayout) view.findViewById(R.id.linear_buy);

        }
    }
}