package com.nusy.rfid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nusy.rfid.R;
import com.nusy.rfid.model.Parking.ResponeParking;

import java.util.List;

public class CustomArrayAdapter extends ArrayAdapter<ResponeParking.Datap> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    List<ResponeParking.Datap> bean;
    private final int mResource;

    public CustomArrayAdapter(@NonNull Context context, @LayoutRes int resource,
                              List<ResponeParking.Datap> bean) {
        super(context, resource, 0, bean);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        this.bean = bean;
    }
    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);

        TextView offTypeTv = (TextView) view.findViewById(R.id.txt_name);
        TextView numOffersTv = (TextView) view.findViewById(R.id.txt_amount);



        offTypeTv.setText(bean.get(position).getName());
        numOffersTv.setText(bean.get(position).getAmount());

        return view;
    }
}