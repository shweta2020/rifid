package com.nusy.rfid.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusy.rfid.R;
import com.nusy.rfid.Activity.TicketBooking.TicketBookingFormActivity;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusys.rfid.model.Data;

import java.util.ArrayList;
import java.util.List;

public class TicketTypeAdapter extends RecyclerView.Adapter<TicketTypeAdapter.MyViewHolder> {
    Context ctx;
    private List<Data> modelList;
    /* int totalAmount;*/
    String Quantity;
    /* int price;*/
    /* String str, s;*/
    List<String> cartList = new ArrayList<>();
    String urlPostCart = "http://13.233.162.24/rifid/api/web_api/add_to_cart";
    SharedPreference_main sharedPreference_main;
    /*   private String[] pickerVals;*/
    // public android.widget.NumberPicker picker1;

    public TicketTypeAdapter(Context ctx, List<Data> modelLists) {
        this.ctx = ctx;
        this.modelList = modelLists;
        sharedPreference_main = SharedPreference_main.getInstance(ctx);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tickeTtype, amount, seats, finalval;
        public int valuePicker1;
        //public NumberPicker numberPicker;

        private String[] pickerVals;
        public NumberPicker picker1;
        int totalAmount;
        public Button proceed, Not_available;
        String str, s;
        int price;

        public MyViewHolder(View view) {
            super(view);

            tickeTtype = (TextView) view.findViewById(R.id.ticketType);
            amount = (TextView) view.findViewById(R.id.amountTicket);
            seats = (TextView) view.findViewById(R.id.seat);
            finalval = (TextView) view.findViewById(R.id.finalamount);

            picker1 = view.findViewById(R.id.numberPicker);
            // numberPicker = (NumberPicker) view.findViewById(R.id.number_picker);
            proceed = view.findViewById(R.id.proceed);
            Not_available = view.findViewById(R.id.Not_available);
            picker1.setMinValue(0);
            picker1.setMaxValue(10);
            pickerVals = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
            int valuePicker1;
            picker1.setDisplayedValues(pickerVals);
           /* numberPicker.setMin(0);
            numberPicker.setMax(100);
            numberPicker.setUnit(1);*/
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.custom_ticket_type, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, final int position) {
        myViewHolder.tickeTtype.setText(modelList.get(position).getTicket());
        //  myViewHolder.finalval.setText(modelList.get(position).getAmount());
        myViewHolder.amount.setText(modelList.get(position).getAmount());
        myViewHolder.seats.setText(modelList.get(position).getSeat());
        myViewHolder.picker1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                //  int valuePicker1 = picker1.getValue();
                myViewHolder.valuePicker1 = myViewHolder.picker1.getValue();
                //  price = Integer.valueOf(bean.getData().get(0).getAmount());
                myViewHolder.price = Integer.valueOf(modelList.get(position).getAmount());
                myViewHolder.s = myViewHolder.pickerVals[myViewHolder.valuePicker1];
                myViewHolder.totalAmount = Integer.valueOf(myViewHolder.s) * myViewHolder.price;
                myViewHolder.finalval.setText(" " + myViewHolder.totalAmount);
                sharedPreference_main.setamt(modelList.get(position).getAmount());
                sharedPreference_main.setEvent_amount("" + myViewHolder.totalAmount);
                sharedPreference_main.setQty((String.valueOf(myViewHolder.s)));
                sharedPreference_main.setticket(modelList.get(position).getTicket_id());
            }
        });
      /*  myViewHolder.numberPicker.setValueChangedListener(new ValueChangedListener() {
            @Override
            public void valueChanged(int value, ActionEnum action) {
                str = myViewHolder.amount.getText().toString();
                price = Integer.valueOf(modelList.get(position).getAmount());
                Quantity = String.valueOf(value);
                totalAmount = value * price;
                myViewHolder.finalval.setText(" " + totalAmount);
                sharedPreference_main.setEvent_amount("" + totalAmount);
                sharedPreference_main.setQty((String.valueOf(value)));
            }
        });*/
        if (modelList.get(position).getAvailability().equals("1")) {

            myViewHolder.proceed.setVisibility(View.VISIBLE);
            myViewHolder.Not_available.setVisibility(View.GONE);

            myViewHolder.proceed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (myViewHolder.totalAmount > 1) {
                        Intent intent = new Intent(ctx, TicketBookingFormActivity.class);
                        ctx.startActivity(intent);
                    } else {
                        Toast.makeText(ctx, "Please Add the Amount", Toast.LENGTH_LONG).show();
                    }
                    /*Intent intent = new Intent(ctx, TicketBookingFormActivity.class);
                    ctx.startActivity(intent);*/
                }
            });
        } else {
            myViewHolder.Not_available.setText(modelList.get(position).getAvailability());
            myViewHolder.Not_available.setVisibility(View.VISIBLE);
            myViewHolder.proceed.setVisibility(View.GONE);
        }
    }
    @Override
    public int getItemCount() {

        return modelList.size();
        //  return 1;
    }
}