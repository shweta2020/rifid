package com.nusy.rfid.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nusy.rfid.Activity.FoodBooking.MyTicketBookingInfoActivity;
import com.nusy.rfid.R;
import com.nusy.rfid.Activity.TicketBooking.Booking_status_Activity;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.model.Ticket_booking_info.ResponeTicketBook;

import java.util.List;

public class Ticket_info_Adapter extends RecyclerView.Adapter<Ticket_info_Adapter.MyViewHolder> {
    Context ctx;
    private List<ResponeTicketBook.Datat> modelList;
    SharedPreference_main sharedPreference_main;

    Adapter_event Adapter;

    public Ticket_info_Adapter(Booking_status_Activity ctx, List<ResponeTicketBook.Datat> modelList) {
        this.ctx = ctx;
        this.modelList = modelList;
        /*sharedPreference_main = SharedPreference_main.getInstance(ctx);*/
    }

    public Ticket_info_Adapter(com.nusy.rfid.TicketBooking.Booking_status_Activity booking_status_activity, List<ResponeTicketBook.Datat> data) {
        this.ctx = ctx;
        this.modelList = modelList;
    }

    @NonNull
    @Override
    public Ticket_info_Adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.booking_info, parent, false);

        return new Ticket_info_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Ticket_info_Adapter.MyViewHolder holder, final int position) {
        holder.tx_time.setText(modelList.get(position).getCreatedAt());
        //  holder.tx_name.setText(modelList.get(position).getName());
        //  holder.tx_event_name.setText(modelList.get(position).getOrderId());
        holder.tx_amount.setText(modelList.get(position).getTotalAmount());
        holder.tx_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, MyTicketBookingInfoActivity.class);
                intent.putExtra("order_id", modelList.get(position).getId());
                ctx.startActivity(intent);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, MyTicketBookingInfoActivity.class);
                intent.putExtra("order_id", modelList.get(position).getId());
                ctx.startActivity(intent);
            }
        });
        Adapter = new Adapter_event(ctx, modelList.get(position).getEventDetail());
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false));
        holder.recyclerView.setItemAnimator(new DefaultItemAnimator());
        holder.recyclerView.setAdapter(Adapter);
     /*   holder.bt_food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                food_booking();
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tx_name, tx_event_name, tx_time, tx_amount, tx_view;
        Button bt_food;
        RecyclerView recyclerView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tx_amount = itemView.findViewById(R.id.txt_amount);
            tx_event_name = itemView.findViewById(R.id.txt_event_name);
            tx_name = itemView.findViewById(R.id.txt_name);
            tx_time = itemView.findViewById(R.id.txt_time);
            recyclerView = itemView.findViewById(R.id.recyclerview);
            tx_view = itemView.findViewById(R.id.view);
            /* bt_food = itemView.findViewById(R.id.btn_food);*/
        }
    }

 /*   private void food_booking() {
        HashMap<String, String> map = new HashMap<>();
        map.put("event_id", "7");

        if (NetworkUtils.isConnected(ctx)) {

            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponeFoodMenu> call = serviceInterface.FoodMenu(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponeFoodMenu>() {
                @Override
                public void onResponse(Call<ResponeFoodMenu> call, Response<ResponeFoodMenu> response) {

//                    printLog(response.body().toString());

                    if (response.isSuccessful()) {

                        ResponeFoodMenu bean = response.body();
                        // printLog(bean.toString());
                        if (bean.getStatus() == 200) {
                            if (bean.getData() != null) {

                              *//*  ticketAdapterinfo = new Adapter_my_ticket_booking(MyTicketBookingInfoActivity.this, bean.getData());
                                recyclerViewTicketinfo.setLayoutManager(new LinearLayoutManager(MyTicketBookingInfoActivity.this, LinearLayoutManager.VERTICAL, false));
                                recyclerViewTicketinfo.setItemAnimator(new DefaultItemAnimator());
                                recyclerViewTicketinfo.setAdapter(ticketAdapterinfo);*//*
                            }
                        } else {
                            showtoast(ctx, "No data found");
                        }
                    } else {
                        showtoast(ctx, "Something is wrong");
                    }
                }

                @Override
                public void onFailure(Call<ResponeFoodMenu> call, Throwable t) {

                }


            });
        } else {

        }
    }*/
}
