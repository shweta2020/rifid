package com.nusy.rfid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusy.rfid.R;
import com.nusy.rfid.model.Ticket_booking_info.ResponeQRCode;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Adapter_QR extends RecyclerView.Adapter<Adapter_QR.myholder> {

    Context ctx;
    private List<ResponeQRCode.Dataqr> modelList;

    public Adapter_QR(Context ctx, List<ResponeQRCode.Dataqr> modelList) {
        this.ctx = ctx;
        this.modelList = modelList;
    }

    @NonNull
    @Override
    public myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.qr_code_list, parent, false);
        return new Adapter_QR.myholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull myholder holder, int position) {
        Picasso.with(ctx).load(modelList.get(position)
                .getFileName())
                .into(holder.QR_code);
        if ((modelList.get(position).getStatus().equals(1))) {
            holder.next.setVisibility(View.VISIBLE);
            holder.QR_code.setVisibility(View.GONE);
        }
        holder.txt_ticket.setText(modelList.get(position).getQrFor());
    }

    @Override
    public int getItemCount() {
        return modelList.size();

    }

    public class myholder extends RecyclerView.ViewHolder {
        ImageView QR_code, next;
        TextView txt_ticket;

        public myholder(@NonNull View itemView) {
            super(itemView);
            QR_code = itemView.findViewById(R.id.img_qr);
            next = itemView.findViewById(R.id.img_disable);
            txt_ticket = itemView.findViewById(R.id.ticket);
        }
    }
}
