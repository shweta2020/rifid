package com.nusy.rfid;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.nusy.rfid.commonMudules.Extension;
import com.nusy.rfid.commonMudules.NetworkUtils;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.retrofit.ApiClient;
import com.nusy.rfid.retrofit.ServiceInterface;
import com.nusy.rfid.utils.DialogsUtils;
import com.nusys.rfid.model.ResponseModel;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusy.rfid.commonMudules.Extension.printLog;
import static com.nusy.rfid.commonMudules.Extension.showtoast;

public class LoginActivity extends AppCompatActivity {

    EditText emailAddres, password;
    String email, passwordValue;
    Button signIn;
    SharedPreferences SM;
    TextView signUp;
    String token = "", id, name, emailaa, image;
    String postUrl = "http://13.233.162.24/rifid/api/web_api/login";
    ProgressDialog progressDialog;
    Dialog dialog;
    SharedPreference_main sharedPreference_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SM = getSharedPreferences("userrecord", 0);
        Boolean islogin = SM.getBoolean("userlogin", false);
        emailAddres = (EditText) findViewById(R.id.txtEmailAddress);
        password = (EditText) findViewById(R.id.txtPassword);
        signUp = (TextView) findViewById(R.id.newAccount);
        dialog = new Dialog(this);

        sharedPreference_main = SharedPreference_main.getInstance(this);

       // sharedPreference_main.setmobile("Hello");

      //  Log.e("device Token", sharedPreference_main.getDeviceToken());
        Toast.makeText(LoginActivity.this, sharedPreference_main.getmobile(), Toast.LENGTH_LONG).show();
        signIn = (Button) findViewById(R.id.button_signIn);

        listener();


    }

    private void listener() {
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLogin();

            }
        });
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);

            }
        });

    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Really Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        LoginActivity.super.onBackPressed();
                    }
                }).create().show();
    }

    private void checkField() {

        if (TextUtils.isEmpty(emailAddres.getText().toString())) {
            emailAddres.setError("field can't be empty");
        } else if (TextUtils.isEmpty(password.getText().toString())) {
            password.setError("field can't be empty");
        } else {
            doLogin();
        }
    }

    private void doLogin() {
        progressDialog = DialogsUtils.showProgressDialog(LoginActivity.this, "Please wait...");
        if (NetworkUtils.isConnected(this)) {
            progressDialog.show();
            HashMap<String, String> map = new HashMap<>();
            map.put("email", emailAddres.getText().toString());
            map.put("password", password.getText().toString());
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponseModel> call = serviceInterface.doLogin(map);
            call.enqueue(new Callback<ResponseModel>() {
                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

//                    printLog(response.body().toString());


                    if (response.isSuccessful()) {
                        progressDialog.show();
                        ResponseModel bean = response.body();

                        printLog(bean.toString());
                        if (bean.getStatus() == 200) {
                            progressDialog.dismiss();
                            sharedPreference_main.setIs_LoggedIn(true);
                            sharedPreference_main.setToken(bean.getToken());
                            sharedPreference_main.setId(Integer.parseInt(bean.getData().get(0).getId()));
                            sharedPreference_main.setemail(bean.getData().get(0).getEmail());
                            sharedPreference_main.setImage(bean.getData().get(0).getImage());
                            sharedPreference_main.setUsername(bean.getData().get(0).getName());
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();
                        }
                    } else {
                        progressDialog.dismiss();
                        showtoast(LoginActivity.this, "Something is wrong");


                    }
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    printLog(t.getMessage());
                    progressDialog.dismiss();
                }
            });
        } else {
            Extension.showErrorDialog(this, dialog);
        }
    }

//    private void checkApi() {
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, postUrl,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//
//                        try {
//
//                            JSONObject jsonObject = new JSONObject(response);
//                            Log.e("TAG", "LEAD_jsonObject" + jsonObject);
//
//                                token=jsonObject.getString("token");
//                            JSONArray jsonArray =  jsonObject.getJSONArray("data");
//
//                            for (int i = 0; i < jsonArray.length(); i++) {
//
//                                JSONObject jsonItemObject = jsonArray.getJSONObject(i);
//
//                          id =      jsonItemObject.getString("id");
//                            name =    jsonItemObject.getString("name");
//                         emailaa=       jsonItemObject.getString("email");
//                          image =       jsonItemObject.getString("image");
//                          Log.e("fffuccccck","succkckck"+id);
//                            }
//                                Log.e("Tag","token Value="+token);
//                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                    intent.putExtra("tokenValue",token);
//                    intent.putExtra("id",id);
//                    intent.putExtra("Name",name);
//                    intent.putExtra("Image",image);
//                    intent.putExtra("email",emailaa);
//                       startActivity(intent);
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            Log.e("JSON Parser", "Error parsing data [" + e.getMessage() + "] " + response);
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e("TAG", "onErrorResponse: " + String.valueOf(error));
//                Toast.makeText(LoginActivity.this, ""+error, Toast.LENGTH_SHORT).show();
//
//            }
//        }) {
//            @Override
//            public Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//
//                params.put("email", email);
//                params.put("password", passwordValue);
//            //    Log.d("TAG", "Does it assign headers?" + apiParamter());
//
//                return params;
//            }
//
//       /*    @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//
//                //This appears in the log
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("", apiParamter());
//                Log.d("TAG", "Does it assign headers?" + apiParamter());
//                return headers;
//            }  */
//        };
//
//        MyApplication.getInstance().addToRequestQueue(stringRequest);
//
//    }
//
//
//    private String apiParamter() {
//        String urlString = null;
//
//
//        try {
//            JSONObject jsonObject = new JSONObject();
//
//           jsonObject.put("username", email);
//            jsonObject.put("password", passwordValue);
//       /*     jsonObject.put("Email", email);
//            jsonObject.put("DOB", dob);
//            jsonObject.put("Gender",genderMBU);
//            jsonObject.put("Mobile", mobile);
//            jsonObject.put("Address", address);
//            jsonObject.put("CityID", cityid);
//            jsonObject.put("PinCode", pincode);
//            jsonObject.put("ProfilePic", profile);
//            jsonObject.put("AnniversaryDate", anniversaryDate);*/
//
//            urlString = jsonObject.toString();
//            Log.e("urlString", "" + urlString);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.e("EXCEPTION>>>", "" + String.valueOf(e));
//
//        }
//        return urlString;
//
//
//
//    }


}
