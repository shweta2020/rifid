package com.nusy.rfid.retrofit;

import com.nusy.rfid.model.Food.ResponeFoodItemStatus;
import com.nusy.rfid.model.Food.ResponeFoodMenu;
import com.nusy.rfid.model.Food.ResponeFoodPayment;
import com.nusy.rfid.model.Food.ResponeFoodStatus;
import com.nusy.rfid.model.Parking.ResponeParking;
import com.nusy.rfid.model.Response_my_booking.ResponeBook;
import com.nusy.rfid.model.Ticket_booking_info.ResponeQRCode;
import com.nusy.rfid.model.Ticket_booking_info.ResponeTicketBook;
import com.nusys.rfid.model.Food.singleFood;
import com.nusys.rfid.model.ResponseModel;
import com.nusys.rfid.model.ResponseRegModle;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ServiceInterface {

    // @FormUrlEncoded
  /*  @POST("login")
    Call<ResponseModel> doLogin(@Field("email") String email, @Field("password") String password);*/


    @POST("login")
    Call<ResponseModel> doLogin(@Body HashMap<String, String> map);


    @POST("login")
    Call<ResponseModel> ownerLogin(@Body HashMap<String, String> map);

    @POST("visitor_registration")
    Call<ResponseRegModle> register(@Body HashMap<String, String> map);

    //@FormUrlEncoded
    @POST("count_cart")
    Call<ResponseModel> getCart(@Field("user_id") int Id);

    //@FormUrlEncoded
   /* @POST("event")
    Call<ResponseModel> getEvents(@Field("user_id") int Id);*/
    @POST("event")
    Call<ResponseModel> getEvents(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

/*
    //@FormUrlEncoded
    @POST("event_date")
    Call<ResponseModel> getEventDate(@Field("event_id") String event_id);

*/

    @POST("event_date")
    Call<ResponseModel> getEventDate(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    /*
        //@FormUrlEncoded
        @POST("event_ticket_list")
        Call<ResponseModel> getEvent_ticket_list(@Field("event_id") String event_id, @Field("date") String date);
        */
    @POST("event_ticket_list")
    Call<ResponseModel> getEvent_ticket_list(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);


    //Spinner Data
    @POST("parking")
    Call<ResponeParking> getspinnerlist(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //Ticket Booking
    @POST("check_out")
    Call<ResponeParking> getticketBook(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    @POST("visitor_ticket_list")
    Call<ResponeTicketBook> getticketinfo(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //Ticket info
    @POST("visitor_ticket_detail")
    Call<ResponeBook> ticketinfo(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //Food Menu list
    @POST("food")
    Call<ResponeFoodMenu> FoodMenu(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //Food
    @POST("single_food")
    Call<singleFood> SingleFood(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //Payment Food
    @POST("food_booking")
    Call<ResponeFoodPayment> Food_payment(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //My Booked food status
    @POST("visitor_food_order_list")
    Call<ResponeFoodStatus> MyFoodStatus(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //My Booked food status details
    @POST("visitor_food_order_detail")
    Call<ResponeFoodItemStatus> MyFoodStatusDetails(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);


    //My QR Code
    @POST("visitor_ticket")
    Call<ResponeQRCode> MyQRCode(@Header("Authorization") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);


}
