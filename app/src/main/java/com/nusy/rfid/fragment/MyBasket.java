package com.nusy.rfid.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.nusy.rfid.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyBasket extends Fragment {
    String event,event_date,time,totalAmount,quantity,price,ticketType;
    TextView eventText,dateofEvent,timeOfEvent,tickt,quant,pricet,total,total2,ppp,qqq;
    Button checkout;

    public MyBasket() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_basket, container, false);

        eventText = (TextView)view.findViewById(R.id.addressOfevent);
        dateofEvent = (TextView)view.findViewById(R.id.dateofevent);
        timeOfEvent = (TextView)view.findViewById(R.id.timeOfevent);
        tickt = (TextView)view.findViewById(R.id.ticketTTTTT);
        quant = (TextView)view.findViewById(R.id.quantity);
        pricet = (TextView)view.findViewById(R.id.price);
        total = (TextView)view.findViewById(R.id.totalt);
        total2 = (TextView)view.findViewById(R.id.total);
        ppp = (TextView)view.findViewById(R.id.ppp);
        qqq = (TextView)view.findViewById(R.id.qqq);
        checkout = view.findViewById(R.id.checkout);


        event_date = getArguments().getString("EventDate");
        event = getArguments().getString("EventName");
        time = getArguments().getString("EventTime");
        totalAmount = getArguments().getString("TotalAmount");
        quantity = getArguments().getString("Quantity");
        price = getArguments().getString("Price");
        ticketType = getArguments().getString("TicketType");

        eventText.setText(event);
        dateofEvent.setText(event_date);
        timeOfEvent.setText(time);
        tickt.setText(ticketType);
        quant.setText(quantity);
        pricet.setText(price);
        total.setText(totalAmount);
        total2.setText(totalAmount);
        ppp.setText(price);
        qqq.setText(quantity);

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Fragment fragment = new Checkout();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.contentmain, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });

        return view;
    }

}
