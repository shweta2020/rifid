package com.nusy.rfid.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.nusy.rfid.R;
import com.nusy.rfid.Activity.TicketBooking.BookTicket;
import com.squareup.picasso.Picasso;


public class EventDetails extends Fragment {
    TextView name, addreess, startDate, time, enddate, amount, visitor,eventvenue;
    Button getTicket;
    ImageView imageView;
    String event_id = "";
    public EventDetails() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.event_details, container, false);

        event_id = getArguments().getString("id");
        Log.e("id value", "" + event_id);
        imageView = view.findViewById(R.id.imagePoster);
        name = view.findViewById(R.id.nameEvent);
        addreess = view.findViewById(R.id.evtaddress);
        startDate = view.findViewById(R.id.evtStartDate);
        time = view.findViewById(R.id.evtTime);
        enddate = view.findViewById(R.id.evtEndDate);
        amount = view.findViewById(R.id.evtAmount);
        visitor = view.findViewById(R.id.evtVisitor);
        getTicket = view.findViewById(R.id.getticket);
        eventvenue=view.findViewById(R.id.evtvenue);
        name.setText(getArguments().getString("name"));
        addreess.setText(getArguments().getString("address"));
        startDate.setText(getArguments().getString("startDate"));
        time.setText(getArguments().getString("time"));
        enddate.setText(getArguments().getString("endDate"));
        amount.setText(getArguments().getString("amount"));
        visitor.setText(getArguments().getString("maxVisitor"));
        eventvenue.setText(getArguments().getString("eventvenue"));

        //Toast.makeText(getContext(),eventvenue.toString(),Toast.LENGTH_LONG).show();



        Picasso.with(getContext()).load(getArguments().getString("image")).fit().centerCrop()
                    .placeholder(R.drawable.event)
                    .error(R.drawable.event)
                .into(imageView);
/*
        Picasso.with(getContext()).load(getArguments().getString("eventimage")).fit().centerCrop()
                .placeholder(R.drawable.event)
                .error(R.drawable.event)
                .into(imageView);*/

        getTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getContext(), BookTicket.class);
                intent.putExtra("event_id", event_id);
                startActivity(intent);

//                Fragment fragment = new BookTicket();
//
//                Bundle bundle = new Bundle();
//                bundle.putString("id",event_id);
//                fragment.setArguments(bundle);
//
//                FragmentManager fragmentManager = ((AppCompatActivity)getActivity()).getSupportFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.contentmain, fragment);
//                fragmentTransaction.addToBackStack(null);
//                fragmentTransaction.commit();

            }
        });

        return view;
    }

}
