package com.nusy.rfid.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.animations.DescriptionAnimation;
import com.glide.slider.library.slidertypes.BaseSliderView;
import com.glide.slider.library.slidertypes.TextSliderView;
import com.glide.slider.library.tricks.ViewPagerEx;
import com.nusy.rfid.R;
import com.nusy.rfid.adapter.TradingAreas_Adapter;
import com.nusy.rfid.model.DataTradingAreasModel;

import java.util.ArrayList;
import java.util.List;

public class Home extends Fragment implements BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener {

    private static final String TAG = Home.class.getSimpleName();
    private SliderLayout mDemoSlider;
    private RecyclerView tradingAreas_recyclerView;
    private List<DataTradingAreasModel> dataTradingAreasModels = new ArrayList<>();
    private TradingAreas_Adapter tradingAreas_adapter;
    private Button eventsBtn;

    public Home() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home, container, false);
        mDemoSlider = view.findViewById(R.id.slider);
        tradingAreas_recyclerView = view.findViewById(R.id.trading_areas_recyclerview);
        eventsBtn = view.findViewById(R.id.eventsBtn);
        slider();
        eventsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new EventS();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.contentmain, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        tradingAreas_adapter = new TradingAreas_Adapter(getActivity(), dataTradingAreasModels);
        tradingAreas_recyclerView.setAdapter(tradingAreas_adapter);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
        tradingAreas_recyclerView.setLayoutManager(manager);
        prepareTradingData();
        return view;
    }

    private void prepareTradingData() {
        int[] covers = new int[]{
                R.drawable.truck,
                R.drawable.tractor,
                R.drawable.cart,
                R.drawable.corporate,
                R.drawable.library,
                R.drawable.healthcare};
        DataTradingAreasModel model = new DataTradingAreasModel(covers[0], "Supply Chain And Logistics");
        dataTradingAreasModels.add(model);

        model = new DataTradingAreasModel(covers[1], "Agriculture Industry");
        dataTradingAreasModels.add(model);

        model = new DataTradingAreasModel(covers[2], "Retail");
        dataTradingAreasModels.add(model);

        model = new DataTradingAreasModel(covers[3], "Corporate Office");
        dataTradingAreasModels.add(model);

        model = new DataTradingAreasModel(covers[4], "Libraries");
        dataTradingAreasModels.add(model);

        model = new DataTradingAreasModel(covers[5], "Healthcare");
        dataTradingAreasModels.add(model);

        tradingAreas_adapter.notifyDataSetChanged();
    }


    private void slider() {

        ArrayList<String> listUrl = new ArrayList<>();
        ArrayList<String> listName = new ArrayList<>();

        listUrl.add("https://assets-global.website-files.com/5b7918ae4c34866ca737694c/5d47be1acc4d3a3adfb873b7_event-info.jpg");
        listName.add("JPG - Event");

        listUrl.add("https://blogmedia.evbstatic.com/wp-content/uploads/wpmulti/sites/3/2016/12/16131147/future-phone-mobile-live-events-technology-trends.png");
        listName.add("PNG - Event");

        listUrl.add("https://blog.nextbee.com/wp-content/uploads/2018/11/corporate-alumni-event.jpg");
        listName.add("GIF - Event");

        listUrl.add("https://www.merchantpaymentsecosystem.com/files/video/bg/videobg-cover.jpg");
        listName.add("WEBP - Event");

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.centerCrop();
        //.diskCacheStrategy(DiskCacheStrategy.NONE)
        //.placeholder(R.drawable.placeholder)
        //.error(R.drawable.placeholder);

        for (int i = 0; i < listUrl.size(); i++) {
            TextSliderView sliderView = new TextSliderView(getContext());
            // if you want show image only / without description text use DefaultSliderView instead

            // initialize SliderLayout
            sliderView
                    .image(listUrl.get(i))
                    .description(listName.get(i))
                    .setRequestOption(requestOptions)
                    .setProgressBarVisible(true)
                    .setOnSliderClickListener(this);

            //add your extra information
            sliderView.bundle(new Bundle());
            sliderView.getBundle().putString("extra", listName.get(i));
            mDemoSlider.addSlider(sliderView);
        }

        // set Slider Transition Animation
        // mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);

        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);
        mDemoSlider.addOnPageChangeListener(this);
        mDemoSlider.stopCyclingWhenTouch(false);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
