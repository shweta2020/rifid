package com.nusy.rfid.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.animations.DescriptionAnimation;
import com.glide.slider.library.slidertypes.BaseSliderView;
import com.glide.slider.library.slidertypes.TextSliderView;
import com.glide.slider.library.tricks.ViewPagerEx;
import com.nusy.rfid.Activity.MainActivity;
import com.nusy.rfid.R;
import com.nusy.rfid.adapter.EventAdapter;
import com.nusy.rfid.adapter.ViewAdapter;
import com.nusy.rfid.model.EventDataModel;
import com.nusy.rfid.model.ViewEventModel;
import com.nusy.rfid.utils.MyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventS extends Fragment implements BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener {
    String token="";

    String postUrlEvent="http://13.233.162.24/rifid/api/web_api/event";

    private SliderLayout mDemoSlider;
    private RecyclerView product_recylerview,viewEvent_recylerview,nextWeekDays_recylerview;
    private ArrayList<EventDataModel> data_model = new ArrayList<>();
    private List<ViewEventModel> data_img_model = new ArrayList<>();
    private EventAdapter mAdapter;
    EventDataModel eventDataModel = new EventDataModel();

    private ViewAdapter viewAdapter;
    public EventS() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.events, container, false);
        viewEvent_recylerview = (RecyclerView) view.findViewById(R.id.viewEvent_recylerview);
        product_recylerview = (RecyclerView) view.findViewById(R.id.product_recylerview);
        nextWeekDays_recylerview = (RecyclerView) view.findViewById(R.id.nextWeekDays_recylerview);
        mDemoSlider = view.findViewById(R.id.slider);

        MainActivity activity = (MainActivity) getActivity();


        token = activity.getMyData();
            Log.e("Tag","values from login"+token);
        eventsApi();



        viewAdapter = new ViewAdapter(getContext(), data_img_model);
        viewEvent_recylerview.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        viewEvent_recylerview.setItemAnimator(new DefaultItemAnimator());
        viewEvent_recylerview.setAdapter(viewAdapter);
        prepareImgData();

        slider();

//        mAdapter = new EventAdapter(getContext(), data_model);
        nextWeekDays_recylerview.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        nextWeekDays_recylerview.setItemAnimator(new DefaultItemAnimator());
        nextWeekDays_recylerview.setAdapter(mAdapter);


       Log.e("tag","cccccocooc"+ eventDataModel.getEvent_address());
        return view;
    }

    private void slider() {


        ArrayList<String> listUrl = new ArrayList<>();
        ArrayList<String> listName = new ArrayList<>();

        listUrl.add("https://assets-global.website-files.com/5b7918ae4c34866ca737694c/5d47be1acc4d3a3adfb873b7_event-info.jpg");
        listName.add("JPG - Event");

        listUrl.add("https://blogmedia.evbstatic.com/wp-content/uploads/wpmulti/sites/3/2016/12/16131147/future-phone-mobile-live-events-technology-trends.png");
        listName.add("PNG - Event");

        listUrl.add("https://blog.nextbee.com/wp-content/uploads/2018/11/corporate-alumni-event.jpg");
        listName.add("GIF - Event");

        listUrl.add("https://www.merchantpaymentsecosystem.com/files/video/bg/videobg-cover.jpg");
        listName.add("WEBP - Event");

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.centerCrop();


        for (int i = 0; i < listUrl.size(); i++) {
            TextSliderView sliderView = new TextSliderView(getContext());
            // if you want show image only / without description text use DefaultSliderView instead

            // initialize SliderLayout
            sliderView
                    .image(listUrl.get(i))
                    .description(listName.get(i))
                    .setRequestOption(requestOptions)
                    .setProgressBarVisible(true)
                    .setOnSliderClickListener(this);

            //add your extra information
            sliderView.bundle(new Bundle());
            sliderView.getBundle().putString("extra", listName.get(i));
            mDemoSlider.addSlider(sliderView);
        }

        // set Slider Transition Animation
        // mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);

        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);
        mDemoSlider.addOnPageChangeListener(this);
        mDemoSlider.stopCyclingWhenTouch(false);
    }

    private void prepareImgData() {
        int[] covers = new int[]{
                R.drawable.event,
                R.drawable.event,
                R.drawable.event,
                R.drawable.event,
                R.drawable.event,
                R.drawable.event};
        ViewEventModel model = new ViewEventModel(covers[0]);
        data_img_model.add(model);

        model = new ViewEventModel(covers[1]);
        data_img_model.add(model);

        model = new ViewEventModel(covers[2]);
        data_img_model.add(model);

        model = new ViewEventModel(covers[3]);
        data_img_model.add(model);

        model = new ViewEventModel(covers[4]);
        data_img_model.add(model);

        model = new ViewEventModel(covers[5]);
        data_img_model.add(model);

        viewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    private void eventsApi() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, postUrlEvent,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            Log.e("TAG", "LEAD_jsonObject" + response);
                            JSONObject object = new JSONObject(response);
                       //     Log.e("tag",""+object.getString("event_name"));
                            JSONArray jsonArray =  object.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                EventDataModel dim = new EventDataModel();
                                JSONObject jsonItemObject = jsonArray.getJSONObject(i);
                                jsonItemObject.getInt("id");
                                jsonItemObject.getString("event_name");
                                jsonItemObject.getString("event_time");
                                jsonItemObject.getString("start_date");
                                jsonItemObject.getString("end_date");
                                jsonItemObject.getString("event_address");
                                jsonItemObject.getString("max_visitor");
                                jsonItemObject.getString("event_type");
                                jsonItemObject.getString("amount");
                                jsonItemObject.getString("image");
                                Log.e("tag",""+ jsonItemObject.getString("event_name"));
                                dim.setId(jsonItemObject.getString("id"));
                                dim.setEvent_name(jsonItemObject.getString("event_name"));
                                dim.setEvent_time(jsonItemObject.getString("event_time"));
                                dim.setStart_date(jsonItemObject.getString("start_date"));
                                dim.setEnd_date(jsonItemObject.getString("end_date"));
                                dim.setEvent_address(jsonItemObject.getString("event_address"));
                                dim.setMax_visitor(jsonItemObject.getString("max_visitor"));
                                dim.setEvent_type(jsonItemObject.getString("event_type"));
                                dim.setAmount(jsonItemObject.getString("amount"));
                                dim.setImage(jsonItemObject.getString("image"));
                                    data_model.add(dim);

//                                mAdapter = new EventAdapter(getContext(), data_model);
//                                product_recylerview.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
//                                product_recylerview.setItemAnimator(new DefaultItemAnimator());
//                                product_recylerview.setAdapter(mAdapter);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("JSON Parser", "Error parsing data [" + e.getMessage() + "] ");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("TAG", "onErrorResponse: " + String.valueOf(error));
                Toast.makeText(getActivity(), ""+error, Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

              /*  params.put("username", email);
                params.put("password", passwordValue);*/
                //    Log.d("TAG", "Does it assign headers?" + apiParamter());

                return params;
            }

          @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                //This appears in the log
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", token);
              //  Log.d("TAG", "Does it assign headers?" + apiParamter());
                return headers;
            }
        };

        MyApplication.getInstance().addToRequestQueue(stringRequest);

    }


/*    private String apiParamter() {
        String urlString = null;


        try {
              JSONObject jsonObject = new JSONObject();

           *//*   jsonObject.put("username", email);
            jsonObject.put("password", passwordValue);
        jsonObject.put("Email", email);
            jsonObject.put("DOB", dob);
            jsonObject.put("Gender",genderMBU);
            jsonObject.put("Mobile", mobile);
            jsonObject.put("Address", address);
            jsonObject.put("CityID", cityid);
            jsonObject.put("PinCode", pincode);
            jsonObject.put("ProfilePic", profile);
            jsonObject.put("AnniversaryDate", anniversaryDate);*//*

            urlString = jsonObject.toString();
            Log.e("urlString", "" + urlString);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("EXCEPTION>>>", "" + String.valueOf(e));

        }
        return urlString;



    }*/



}
