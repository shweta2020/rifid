package com.nusy.rfid.fragment;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nusy.rfid.Activity.MainActivity;
import com.nusy.rfid.R;
import com.nusy.rfid.adapter.CartAdapter;
import com.nusy.rfid.model.CartModel;
import com.nusy.rfid.utils.MyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Cart extends Fragment {

    String cartUrl = "http://13.233.162.24/rifid/api/web_api/cart";
    private ArrayList<CartModel> cartModels = new ArrayList<>();
    CartAdapter cartAdapter;
    RecyclerView recyclerViewCart;
    String id;
    Button pay;
    String price = "";
    LinearLayout linearLayout;
    ImageView cartImg;

    public Cart() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View  view =  inflater.inflate(R.layout.fragment_cart, container, false);
        ticketsApi();
        MainActivity activity = (MainActivity) getActivity();
        id = activity.getIDData();

        Log.e("qaswdef","id in cart "+id);
        recyclerViewCart = (RecyclerView)view.findViewById(R.id.recyclerCart);
        linearLayout = (LinearLayout)view.findViewById(R.id.linearqw);



   /*     if(!cartModels.isEmpty())
        {

            linearLayout.setVisibility(View.INVISIBLE);
            cartImg.setVisibility(View.VISIBLE);



        }
*/

        return view;

    }



    private void ticketsApi() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, cartUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            Log.e("TAG", "LEAD_jsonObject" + response);
                            JSONObject object = new JSONObject(response);
                           price = object.getString("total_amount");
                            pay.setText("Payment"+"\u20B9"+object.getString("total_amount"));
                            Log.e("sdfvbn","message in Crat.java"+object.getString("total_amount"));
                            JSONArray jsonArray =  object.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                CartModel dd = new CartModel();
                                JSONObject jsonItemObject = jsonArray.getJSONObject(i);

                                jsonItemObject.getString("event_name");
                                jsonItemObject.getString("event_venue");
                                jsonItemObject.getString("event_time");
                                jsonItemObject.getString("event_date");
                                jsonItemObject.getString("ticket");
                                jsonItemObject.getString("sub_total");


                                dd.setId(jsonItemObject.getString("id"));
                                dd.setEventName(jsonItemObject.getString("event_name"));
                                dd.setEventTime(jsonItemObject.getString("event_time"));
                                dd.setEventVenue(jsonItemObject.getString("event_venue"));
                                dd.setTicket(jsonItemObject.getString("ticket"));
                                dd.setTotal(jsonItemObject.getString("sub_total"));
                                dd.setEventDate(jsonItemObject.getString("event_date"));

                                cartModels.add(dd);
                                cartAdapter = new CartAdapter(getContext(), cartModels);
                                recyclerViewCart.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                recyclerViewCart.setItemAnimator(new DefaultItemAnimator());
                                recyclerViewCart.setAdapter(cartAdapter);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("JSON Parser", "Error parsing data [" + e.getMessage() + "] ");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("TAG", "onErrorResponse: " + String.valueOf(error));
                Toast.makeText(getActivity(), ""+error, Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", id);
                //    Log.d("TAG", "Does it assign headers?" + apiParamter());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                //This appears in the log
                HashMap<String, String> headers = new HashMap<String, String>();
                //   headers.put("Content-Type", "application/json");
                //  headers.put("Authorization", token);
                //  Log.d("TAG", "Does it assign headers?" + apiParamter());
                return headers;
            }
        };

        MyApplication.getInstance().addToRequestQueue(stringRequest);

    }


}
