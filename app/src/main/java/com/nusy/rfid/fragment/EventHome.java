package com.nusy.rfid.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.animations.DescriptionAnimation;
import com.glide.slider.library.slidertypes.BaseSliderView;
import com.glide.slider.library.slidertypes.TextSliderView;
import com.glide.slider.library.tricks.ViewPagerEx;
import com.nusy.rfid.R;
import com.nusy.rfid.adapter.EventAdapter;
import com.nusy.rfid.adapter.ViewAdapter;
import com.nusy.rfid.commonMudules.Extension;
import com.nusy.rfid.commonMudules.NetworkUtils;
import com.nusy.rfid.commonMudules.SharedPreference_main;
import com.nusy.rfid.model.EventDataModel;
import com.nusy.rfid.model.ViewEventModel;
import com.nusy.rfid.retrofit.ApiClient;
import com.nusy.rfid.retrofit.ServiceInterface;
import com.nusy.rfid.utils.DialogsUtils;
import com.nusys.rfid.model.Data;
import com.nusys.rfid.model.ResponseModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusy.rfid.commonMudules.Extension.printLog;
import static com.nusy.rfid.commonMudules.Extension.showtoast;
import static com.nusy.rfid.utils.Constants.Content_Type;

public class EventHome extends Fragment implements BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener {
    String token = "";

    String postUrlEvent = "http://13.233.162.24/rifid/api/web_api/event";

    private SliderLayout mDemoSlider;
    private RecyclerView product_recylerview, viewEvent_recylerview, nextWeekDays_recylerview;
    private List<Data> data_model = new ArrayList<>();
    private List<ViewEventModel> data_img_model = new ArrayList<>();
    private EventAdapter mAdapter;
    EventDataModel eventDataModel = new EventDataModel();
    ProgressDialog progressDialog;
    private ViewAdapter viewAdapter;
    SharedPreference_main sharedPreference_main;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private Dialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.events, container, false);
        viewEvent_recylerview = (RecyclerView) view.findViewById(R.id.viewEvent_recylerview);
        product_recylerview = (RecyclerView) view.findViewById(R.id.product_recylerview);
        nextWeekDays_recylerview = (RecyclerView) view.findViewById(R.id.nextWeekDays_recylerview);
        mDemoSlider = view.findViewById(R.id.slider);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_home);
        init();
        getEvent();
//        MainActivity activity = (MainActivity) getActivity();
//        token = activity.getMyData();
//        Log.e("Tag", "values from login" + token);

        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getEvent();
                        // progressBar.setVisibility(View.VISIBLE);
                    }
                }
        );


        return view;
    }

    private void init() {

        sharedPreference_main = SharedPreference_main.getInstance(getContext());
        dialog = new Dialog(getContext());

        viewAdapter = new ViewAdapter(getContext(), data_img_model);
        viewEvent_recylerview.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        viewEvent_recylerview.setItemAnimator(new DefaultItemAnimator());
        viewEvent_recylerview.setAdapter(viewAdapter);
        prepareImgData();

        slider();
        //getEvent();

////        mAdapter = new EventAdapter(getContext(), data_model);
//        nextWeekDays_recylerview.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
//        nextWeekDays_recylerview.setItemAnimator(new DefaultItemAnimator());
//        nextWeekDays_recylerview.setAdapter(mAdapter);


    }

    private void prepareImgData() {
        int[] covers = new int[]{
                R.drawable.event,
                R.drawable.event,
                R.drawable.event,
                R.drawable.event,
                R.drawable.event,
                R.drawable.event};
        ViewEventModel model = new ViewEventModel(covers[0]);
        data_img_model.add(model);

        model = new ViewEventModel(covers[1]);
        data_img_model.add(model);

        model = new ViewEventModel(covers[2]);
        data_img_model.add(model);

        model = new ViewEventModel(covers[3]);
        data_img_model.add(model);

        model = new ViewEventModel(covers[4]);
        data_img_model.add(model);

        model = new ViewEventModel(covers[5]);
        data_img_model.add(model);

        viewAdapter.notifyDataSetChanged();
    }

    private void slider() {


        ArrayList<String> listUrl = new ArrayList<>();
        ArrayList<String> listName = new ArrayList<>();

        listUrl.add("https://assets-global.website-files.com/5b7918ae4c34866ca737694c/5d47be1acc4d3a3adfb873b7_event-info.jpg");
        listName.add("JPG - Event");

        listUrl.add("https://blogmedia.evbstatic.com/wp-content/uploads/wpmulti/sites/3/2016/12/16131147/future-phone-mobile-live-events-technology-trends.png");
        listName.add("PNG - Event");

        listUrl.add("https://blog.nextbee.com/wp-content/uploads/2018/11/corporate-alumni-event.jpg");
        listName.add("GIF - Event");

        listUrl.add("https://www.merchantpaymentsecosystem.com/files/video/bg/videobg-cover.jpg");
        listName.add("WEBP - Event");

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.centerCrop();


        for (int i = 0; i < listUrl.size(); i++) {
            TextSliderView sliderView = new TextSliderView(getContext());
            // if you want show image only / without description text use DefaultSliderView instead

            // initialize SliderLayout
            sliderView
                    .image(listUrl.get(i))
                    .description(listName.get(i))
                    .setRequestOption(requestOptions)
                    .setProgressBarVisible(true)
                    .setOnSliderClickListener(this);

            //add your extra information
            sliderView.bundle(new Bundle());
            sliderView.getBundle().putString("extra", listName.get(i));
            mDemoSlider.addSlider(sliderView);
        }

        // set Slider Transition Animation
        // mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);

        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);
        mDemoSlider.addOnPageChangeListener(this);
        mDemoSlider.stopCyclingWhenTouch(false);
    }


    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    private void getEvent() {
        progressDialog = DialogsUtils.showProgressDialog(getActivity(), "Please wait...");
        HashMap<String, String> map = new HashMap<>();
        //map.put("gender", gender);
        if (NetworkUtils.isConnected(getContext())) {
            progressDialog.show();
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ResponseModel> call = serviceInterface.getEvents(sharedPreference_main.getToken(), Content_Type, map);
            call.enqueue(new Callback<ResponseModel>() {
                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

//                    printLog(response.body().toString());


                    if (response.isSuccessful()) {

                        ResponseModel bean = response.body();
                        printLog(bean.toString());
                        if (bean.getStatus() == 200) {
                            progressDialog.dismiss();
                            mSwipeRefreshLayout.setRefreshing(false);
                            mAdapter = new EventAdapter(getContext(), bean.getData());
                            product_recylerview.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                            product_recylerview.setItemAnimator(new DefaultItemAnimator());
                            product_recylerview.setAdapter(mAdapter);
                            nextWeekDays_recylerview.setAdapter(mAdapter);
//                          totalitem= String.valueOf(bean.getData().get(0).getTotal_items());
                        }

                    } else {
                        showtoast(getContext(), "Something is wrong");
                        mSwipeRefreshLayout.setRefreshing(false);
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    // printLog(t.getMessage());
                    mSwipeRefreshLayout.setRefreshing(false);
                    progressDialog.dismiss();
                }
            });
        } else {

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            }, 2800);
            mSwipeRefreshLayout.setRefreshing(false);
            Extension.showErrorDialog(getContext(), dialog);
        }

    }
}
